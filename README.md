# Project "Social Network"  

**Functionality:**  

+ registration  
+ display profile  
+ edit profile  
+ upload profile picture  
+ authentication with Spring Security  
+ AJAX search autocomplete  
+ AJAX search with pagination  
+ users export to XML / import from XML  
+ messaging through WebSocket  
+ posting on wall  
+ groups creation  
+ adding/removing friends  

**Tools:**  
JDK 7, 8, Spring 5, JPA 2 / Hibernate 5, XStream, jQuery 3, Twitter Bootstrap 4, JUnit 4, Mockito, Maven 3, Git / Bitbucket, Tomcat 7, MySQL, IntelliJIDEA 2018.  
**Notes:**
MySQL ddl is located in the db/ddl.sql

**Screenshoots:**  
![picture](screens/community.png)
![picture](screens/editpage.png)
![picture](screens/loginform.png)
![picture](screens/mainpage.png)

The project hosted at [https://sudarevnetwork.herokuapp.com/](https://sudarevnetwork.herokuapp.com/)

user: test@test.ru

password: 123qweASD

_  
**Ruslan Sudarev**  
Training getJavaJob  
http://www.getjavajob.com
