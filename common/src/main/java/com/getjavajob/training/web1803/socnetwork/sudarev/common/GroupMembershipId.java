package com.getjavajob.training.web1803.socnetwork.sudarev.common;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class GroupMembershipId implements Serializable {

    @Column(name = "group_Id")
    private Integer groupId;
    @Column(name = "account_Id")
    private Integer accountId;

    public GroupMembershipId() {
    }

    public GroupMembershipId(Integer groupId, Integer accountId) {
        this.accountId = accountId;
        this.groupId = groupId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass())
            return false;

        GroupMembershipId that = (GroupMembershipId) o;
        return Objects.equals(groupId, that.groupId) &&
                Objects.equals(accountId, that.accountId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupId, accountId);
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }
}
