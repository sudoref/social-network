package com.getjavajob.training.web1803.socnetwork.sudarev.common;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "group_membership")
public class Membership {

    @EmbeddedId
    private GroupMembershipId id;
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("groupId")
    @Fetch(FetchMode.JOIN)
    private Group group;
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("accountId")
    @Fetch(FetchMode.JOIN)
    private Account account;
    @Column(name = "status")
    private Integer status;

    public Membership() {
    }

    public Membership(Account account, Group group, Integer status) {
        this.status = status;
        this.account = account;
        this.group = group;
        this.id = new GroupMembershipId(group.getGroupID(), account.getAccountId());
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Membership{" +
                "id=" + id +
                ", group=" + group +
                ", account=" + account +
                ", status=" + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Membership that = (Membership) o;
        return Objects.equals(group, that.group) &&
                Objects.equals(account, that.account);
    }

    @Override
    public int hashCode() {
        return Objects.hash(group, account);
    }
}
