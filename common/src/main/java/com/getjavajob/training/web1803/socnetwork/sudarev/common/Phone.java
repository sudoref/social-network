package com.getjavajob.training.web1803.socnetwork.sudarev.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import javax.persistence.*;

@Entity
@XStreamAlias("phone")
@Table(name = "phones")
public class Phone {

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public enum Type {
        MOB, WORK, HOME
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer phoneId;
    @Enumerated(EnumType.STRING)
    private Type type;
    private Long number;
    @ManyToOne
    @XStreamOmitField
    @JoinColumn(name = "accountId", nullable = false)
    private Account account;

    public Phone(long number, Type type, Account account) {
        this.number = number;
        this.type = type;
        this.account = account;
    }

    public Phone() {
    }

    public Integer getPhoneId() {
        return phoneId;
    }

    public void setPhoneID(Integer phoneId) {
        this.phoneId = phoneId;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "number=" + number +
                ", type='" + type + '\'' +
                ", accountId=" + account.getAccountId() +
                '}';
    }
}
