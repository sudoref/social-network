package com.getjavajob.training.web1803.socnetwork.sudarev.common;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "relationship")
public class Relationship implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;
    @Column(name = "account_one_id")
    private Integer account1;
    @Column(name = "account_two_id")
    private Integer account2;
    private Integer status;
    @Column(name = "action_account_id")
    private Integer initiator;

    public Relationship() {
    }

    public Integer getAccount1() {
        return account1;
    }

    public void setAccount1(Integer account1) {
        this.account1 = account1;
    }

    public Integer getAccount2() {
        return account2;
    }

    public void setAccount2(Integer account2) {
        this.account2 = account2;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Relationship(Integer account1, Integer account2, Integer status, Integer initiator) {
        if (account1 < account2) {
            this.account1 = account1;
            this.account2 = account2;
        } else {
            this.account1 = account2;
            this.account2 = account1;
        }
        this.status = status;
        this.initiator = initiator;
    }

    public Integer getInitiator() {
        return initiator;
    }

    public void setInitiator(Integer initiator) {
        this.initiator = initiator;
    }

    @Override
    public String toString() {
        return "Relationship{" +
                "account1=" + account1 +
                ", account2=" + account2 +
                ", status=" + status +
                ", initiator=" + initiator +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Relationship that = (Relationship) o;
        return Objects.equals(account1, that.account1) &&
                Objects.equals(account2, that.account2);
    }

    @Override
    public int hashCode() {

        return Objects.hash(account1, account2);
    }
}
