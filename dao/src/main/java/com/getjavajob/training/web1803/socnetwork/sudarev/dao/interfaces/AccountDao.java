package com.getjavajob.training.web1803.socnetwork.sudarev.dao.interfaces;



import com.getjavajob.training.web1803.socnetwork.sudarev.common.Account;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface AccountDao extends CrudRepository<Account, Integer> {

    Account getAccountByEmail(String email);

    Account getAccountByAccountId(Integer id);

    int countAccountsByEmail(String email);

    @Query("Select u FROM Account u WHERE u.firstName LIKE ?1 OR u.midName LIKE ?1 OR u.surname LIKE ?1 OR u.email LIKE ?1")
    List<Account> getByPage(String pattern, Pageable pageable);

    @Query("Select u FROM Account u WHERE u.firstName LIKE ?1 OR u.midName LIKE ?1 OR u.surname LIKE ?1 OR u.email LIKE ?1")
    List<Account> getByPattern(String pattern);
}
