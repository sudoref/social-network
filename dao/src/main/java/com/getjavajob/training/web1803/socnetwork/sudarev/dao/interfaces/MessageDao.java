package com.getjavajob.training.web1803.socnetwork.sudarev.dao.interfaces;

import com.getjavajob.training.web1803.socnetwork.sudarev.common.Message;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MessageDao extends CrudRepository<Message, Integer> {

    List<Message> getAllByTypeAndConversation(Message.Type type, String conversation);
}
