package com.getjavajob.training.web1803.socnetwork.sudarev.dao.interfaces;


import com.getjavajob.training.web1803.socnetwork.sudarev.common.Group;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface GroupDao extends CrudRepository<Group, Integer> {

    Group getByName(String name);

    Integer countAllByNameLike(String pattern);

    List<Group> getAllByNameLike(String searchPattern, Pageable pageable);

    List<Group> getAllByNameLike(String searchPattern);
}
