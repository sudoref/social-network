package com.getjavajob.training.web1803.socnetwork.sudarev.dao.interfaces;

import com.getjavajob.training.web1803.socnetwork.sudarev.common.Relationship;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RelationshipDao extends CrudRepository<Relationship, Integer> {

    Relationship getByAccount1AndAccount2(int account1, int account2);

    @Modifying
    @Query("DELETE FROM Relationship r WHERE r.account1 = ?1 AND r.account2 = ?2")
    void delete(int account1, int account2);

    @Query("SELECT r FROM Relationship r WHERE r.account1 = ?1 OR r.account2=?1")
    List<Relationship> getAllByAccount(Integer AccountId);
}
