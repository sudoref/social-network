import com.getjavajob.training.web1803.socnetwork.sudarev.common.Message;
import com.getjavajob.training.web1803.socnetwork.sudarev.dao.interfaces.MessageDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-test.xml"})
@SqlGroup({
        @Sql(executionPhase = BEFORE_TEST_METHOD, scripts = {"classpath:data-model.sql", "classpath:fillDb.sql"}),
        @Sql(executionPhase = AFTER_TEST_METHOD, scripts = "classpath:remove.sql")
})
public class MessageDaoTest {

    @Autowired
    MessageDao dao;

    @Test
    public void getByKeyTest() {
        Message message = dao.findById(1).orElseThrow(NullPointerException::new);
        assertEquals("1:2", message.getConversation());
    }

    @Test
    public void getByValueTest() {
        Message message = new Message();
        message.setConversation("1:2");
        message.setType(Message.Type.PRIVATE);
        List<Message> messages = dao.getAllByTypeAndConversation(message.getType(), message.getConversation());
        assertEquals(1, messages.size());
    }

    @Test
    public void persistTest() {
        Message message = new Message();
        message.setConversation("1:2");
        message.setType(Message.Type.PRIVATE);
        assertEquals(2, dao.getAllByTypeAndConversation(Message.Type.PRIVATE, "1:2").size());
    }

    @Test(expected = NullPointerException.class)
    public void deleteTest() {
        Message message = dao.findById(1).orElseThrow(NullPointerException::new);
        dao.delete(message);
        assertNull(dao.findById(1).orElseThrow(NullPointerException::new));
    }
}
