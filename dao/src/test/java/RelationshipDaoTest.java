import com.getjavajob.training.web1803.socnetwork.sudarev.common.Relationship;
import com.getjavajob.training.web1803.socnetwork.sudarev.dao.interfaces.RelationshipDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-test.xml"})
@SqlGroup({
        @Sql(executionPhase = BEFORE_TEST_METHOD, scripts = {"classpath:data-model.sql", "classpath:fillDb.sql"}),
        @Sql(executionPhase = AFTER_TEST_METHOD, scripts = "classpath:remove.sql")
})
public class RelationshipDaoTest {

    @Autowired
    RelationshipDao dao;

    @Test
    public void getAllByValue() {
        assertEquals(1, dao.getAllByAccount(2).size());
    }

    @Test
    public void persistTest() {
        dao.save(new Relationship(1, 3, 3, 1));
        assertEquals(1, dao.getAllByAccount(2).size());
    }

    @Test
    public void updateTest() {
        Relationship relationship = dao.getAllByAccount(1).get(0);
        relationship.setStatus(3);
        dao.save(relationship);
    }
}
