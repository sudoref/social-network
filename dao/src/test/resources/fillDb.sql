INSERT INTO GROUPS (ID, name, description, creator, avatar)
VALUES
  (1, 'testgroup', 'test group description', 'ivan@mail.ru', NULL),
  (2, 'testgroup2', 'test group2 description', 'ivan@mail.ru', NULL);

INSERT INTO ACCOUNTS (ID, firstName, midName, surname, birthDate, homeAddress, workAddress, email, icq, skype, gender, password, isAdmin, avatar)
VALUES
  (1, 'Ivan', 'Ivanovich', 'Ivanov', '2012-11-10', '123 Lenin st., apt. 4, Moscow ',
      '321 Pushkin st., office 212, Moscow', 'ivan@mail.ru', 123321, 'vanya', 'male', 123, 0, NULL),
  (2, 'Vasya', 'Petrovich', 'Vlasov', '1990-11-20', '10 corp 1 Vernost st., apt. 92, SPb', NULL, 'vasoyk@mail.ru', NULL,
      'vasyok', 'male', 123, 0, NULL),
  (3, 'Marina', 'Alexandrovna', 'Kolhozova', '1992-05-16', '121 Pushkin st., apt. 8, SPb',
      '11 Lenin st., office 66, SPb', 'marya@mail.ru', 079917, 'mayar', 'female', 123, 0, NULL);


INSERT INTO MESSAGES (ID, CONVERSATIONID, TYPE, SENDER, DATE, message, RECIPIENT, IMAGE)
VALUES
  (1, '1:2', 'PRIVATE', 1, '2018-07-21 18:43:50', 'HI', 2, NULL);

INSERT INTO PHONES (type, number, accountID) VALUES
  ('WORK', 12332111, 1),
  ('HOME', 915234921, 1),
  ('MOB', 912323112, 1),
  ('WORK', 8881122, 2),
  ('MOB', 9657554338, 2),
  ('MOB', 991122123, 3);

INSERT INTO RELATIONSHIP (ACCOUNT_ONE_ID, ACCOUNT_TWO_ID, STATUS, ACTION_ACCOUNT_ID)
VALUES
  (1, 2, 1, 1);