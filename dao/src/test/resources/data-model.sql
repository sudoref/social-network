CREATE TABLE IF NOT EXISTS ACCOUNTS (
  ID               INT          NOT NULL AUTO_INCREMENT,
  firstName        VARCHAR(255) NOT NULL,
  midName          VARCHAR(255),
  surname          VARCHAR(255),
  birthDate        DATE,
  homeAddress      VARCHAR(255),
  workAddress      VARCHAR(255),
  email            VARCHAR(255) NOT NULL,
  icq              BIGINT,
  skype            VARCHAR(255),
  gender           VARCHAR(255) NOT NULL,
  password         VARCHAR(255),
  registrationDate DATE,
  isAdmin          INT,
  avatar           BLOB,
  PRIMARY KEY (ID),
  UNIQUE (email)
);

CREATE TABLE IF NOT EXISTS GROUPS (
  ID          INT         NOT NULL AUTO_INCREMENT,
  description VARCHAR(45),
  creator     VARCHAR(45),
  name        VARCHAR(45) NOT NULL,
  avatar      BLOB,
  PRIMARY KEY (`ID`, `name`),
  UNIQUE (ID),
  UNIQUE (name)
);

CREATE TABLE IF NOT EXISTS GROUP_MEMBERSHIP
(
  group_Id   INT,
  account_Id INT,
  status     INT,
);

CREATE TABLE IF NOT EXISTS PHONES (
  ID        INT NOT NULL AUTO_INCREMENT,
  type      ENUM ('MOB', 'WORK', 'HOME'),
  number    BIGINT,
  accountID INT,
  FOREIGN KEY (accountID) REFERENCES ACCOUNTS (ID) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS RELATIONSHIP
(
  ID                 INT          NOT NULL AUTO_INCREMENT,
  account_one_id    INT,
  account_two_id    INT,
  status            INT,
  action_account_id INT,
);

CREATE TABLE IF NOT EXISTS MESSAGES (
  conversationID VARCHAR(45),
  type           ENUM ('PRIVATE', 'GROUP', 'POST'),
  sender         INT(11),
  date           DATETIME,
  message        TEXT,
  recipient      INT,
  image          LONGBLOB,
  ID             INT NOT NULL AUTO_INCREMENT,
  CONSTRAINT MESSAGES_ACCOUNTS_ID_fk FOREIGN KEY (sender) REFERENCES ACCOUNTS (ID) ON DELETE CASCADE ON UPDATE CASCADE
);