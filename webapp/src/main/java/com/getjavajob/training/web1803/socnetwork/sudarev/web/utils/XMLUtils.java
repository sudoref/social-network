package com.getjavajob.training.web1803.socnetwork.sudarev.web.utils;

import com.getjavajob.training.web1803.socnetwork.sudarev.common.Account;
import com.getjavajob.training.web1803.socnetwork.sudarev.common.Phone;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.xml.sax.SAXException;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;
import java.io.StringReader;

import static java.lang.Thread.currentThread;
import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;

public class XMLUtils {
    private static final String VALIDATION_SCHEMA = "account-schema.xsd";

    public static void validate(String xml) throws IOException, SAXException {
        Source xmlSource = new StreamSource(new StringReader(xml));
        Source xsdSource = new StreamSource(currentThread()
                .getContextClassLoader()
                .getResourceAsStream(VALIDATION_SCHEMA));
        SchemaFactory schemaFactory = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
        Schema schema = schemaFactory.newSchema(xsdSource);
        Validator validator = schema.newValidator();
        validator.validate(xmlSource);
    }

    public static XStream getAccountXStream() {
        XStream xstream = new XStream(new DomDriver());
        xstream.processAnnotations(Account.class);
        xstream.processAnnotations(Phone.class);
        Class<?>[] classes = new Class[]{Account.class, Phone.class};
        XStream.setupDefaultSecurity(xstream);
        xstream.allowTypes(classes);
        return xstream;
    }
}