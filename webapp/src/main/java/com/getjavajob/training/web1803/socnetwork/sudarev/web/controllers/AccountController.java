package com.getjavajob.training.web1803.socnetwork.sudarev.web.controllers;

import com.getjavajob.training.web1803.socnetwork.sudarev.common.*;
import com.getjavajob.training.web1803.socnetwork.sudarev.service.AccountService;
import com.getjavajob.training.web1803.socnetwork.sudarev.service.MessageService;
import com.getjavajob.training.web1803.socnetwork.sudarev.service.RelationshipService;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpSession;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.util.*;

import static com.getjavajob.training.web1803.socnetwork.sudarev.web.utils.XMLUtils.getAccountXStream;
import static com.getjavajob.training.web1803.socnetwork.sudarev.web.utils.XMLUtils.validate;
import static java.lang.Long.*;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;


@Controller
public class AccountController {

    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);
    @Autowired
    private RelationshipService relationshipService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private HttpSession session;

    @InitBinder
    protected void initBinder(ServletRequestDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
    }

    @RequestMapping(value = "/account")
    public ModelAndView accountPage(@RequestParam(value = "edit", required = false) String edit,
                                    @RequestParam(value = "id", required = false) int id) {
        logger.info("Received GET request to show account page. Id = {}", id);
        Account account = accountService.getAccountById(id);
        ModelAndView userPage = new ModelAndView();
        if (isNull(account)) {
            userPage.setViewName("stub");
            return userPage;
        } else {
            userPage.addObject(account);
        }
        Account sessionAcc = (Account) session.getAttribute("sessionAcc");
        List<Message> posts = messageService.getAllPostMessagesFromAccount(id);
        Collections.reverse(posts);
        Map<Message, Account> postMap = new LinkedHashMap<>();
        for (Message post : posts) {
            if (isNull(post.getImage()) || post.getImage().length == 0) {
                post.setImage(null);
            }
            Account writer = post.getSender();
            postMap.put(post, writer);
        }
        userPage.addObject("posts", postMap);
        createFriendList(userPage, account, sessionAcc.getAccountId());
        List<Group> groups = new ArrayList<>();
        for (Membership membership : account.getGroups()) {
            groups.add(membership.getGroup());
        }
        userPage.addObject("groups", groups);
        userPage.addObject(account);

        if (account.getPhones().size() > 0) {
            for (Phone phone : account.getPhones()) {
                int i = account.getPhones().indexOf(phone);
                userPage.addObject("type" + i, phone.getType().name());
                userPage.addObject("phone" + i, phone.getNumber());
            }
        }
        if (nonNull(edit)) {
            userPage.setViewName("editprofile");
            return userPage;
        }
        if (sessionAcc.getAccountId() == id) {
            userPage.setViewName("home");
            return userPage;
        } else {
            userPage.setViewName("guest");
            return userPage;
        }
    }

    @RequestMapping(value = "/errorAccess")
    public String errorAccessPage() {
        return "403";
    }

    @RequestMapping(value = "/editProfile", method = RequestMethod.POST)
    public ModelAndView editAccount(@ModelAttribute Account account,
                                    @RequestParam(value = "birthDay", required = false) String birthDate,
                                    @RequestParam(value = "photo", required = false) MultipartFile photo,
                                    @RequestParam(value = "isAdmin", required = false) String isAdmin) {
        logger.info("Edit account page. Id = {}", account.getAccountId());
        List<Phone> phones = new ArrayList<>();
        if (nonNull(account.getPhones())) {
            for (Phone phone : account.getPhones()) {
                phone.setAccount(account);
                if (nonNull(phone.getNumber()) && nonNull(phone.getType())) {
                    phones.add(phone);
                }
            }
            account.setPhones(phones);
        }
        if (nonNull(isAdmin) && isAdmin.equals("on")) {
            account.setAdmin(true);
        } else {
            account.setAdmin(false);
        }
        if (nonNull(birthDate) && !birthDate.isEmpty()) {
            account.setBirthDate(LocalDate.parse(birthDate));
        }
        try {
            if (nonNull(photo) &&
                    photo.getContentType().equals("image/jpeg") ||
                    photo.getContentType().equals("image/png")) {
                account.setAvatar(IOUtils.toByteArray(new BufferedInputStream((photo.getInputStream()))));
            } else {
                account.setAvatar(accountService.getAccountById(account.getAccountId()).getAvatar());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        accountService.update(account);
        return new ModelAndView("redirect:/");
    }

    @RequestMapping("/deleteProfile")
    public String delete(@RequestParam("accountId") int id) {
        logger.info("Delete account page. Id = {}", id);
        Account account = accountService.getAccountById(id);
        Account sessionAcc = (Account) session.getAttribute("sessionAcc");
        if (nonNull(account)) {
            accountService.deleteAccount(account);
            if (account.getAccountId().equals(sessionAcc.getAccountId())) {
                return "redirect:/logout";
            } else {
                return "redirect:/account?id=" + sessionAcc.getAccountId();
            }
        }
        return "redirect:/index";
    }

    @RequestMapping(value = "/registrationNewUser", method = RequestMethod.POST)
    public ModelAndView registrationNewUser(@ModelAttribute Account account,
                                            @RequestParam(value = "birthDay", required = false) String birthDate,
                                            @RequestParam(value = "type", required = false) String type,
                                            @RequestParam(value = "phone", required = false) String phone) {
        logger.info("Create account page with Email = {}", account.getEmail());
        List<Phone> phones = new ArrayList<>();
        if (!type.isEmpty() && !phone.isEmpty()) {
            phones.add(new Phone(valueOf(phone),
                    Phone.Type.valueOf(type), account));
        }
        if (!birthDate.isEmpty()) {
            account.setBirthDate(LocalDate.parse(birthDate));
        }
        account.setPhones(phones);
        account.setRegistrationDate(LocalDate.now());
        account.setAdmin(false);
        if (nonNull(accountService.createAccount(account))) {
            logger.info("User with email" + account.getEmail() + " has created");
            return new ModelAndView("redirect:/");
        } else {
            logger.error("User with email" + account.getEmail() + " address is already registered");
            ModelAndView modelAndView = new ModelAndView("redirect:/registration");
            modelAndView.addObject("errorMessage", "User with this email address is already registered");
            return modelAndView;
        }
    }

    private void createFriendList(ModelAndView userPage, Account account, int sessionAccountId) {
        List<Account> friends = new ArrayList<>();
        account.setRelationships(relationshipService.getAllRelationships(account));
        if (nonNull(account.getRelationships()) && !account.getRelationships().isEmpty()) {
            Map<Integer, int[]> relationships = account.getRelationships();
            if (relationships.containsKey(sessionAccountId) &&
                    relationships.get(sessionAccountId)[1] == sessionAccountId &&
                    relationships.get(sessionAccountId)[0] == 1) {
                userPage.addObject("friendStatus", "initiator");
            } else if (relationships.containsKey(sessionAccountId) &&
                    relationships.get(sessionAccountId)[0] > 1) {
                userPage.addObject("friendStatus", "friend");
            } else if (relationships.containsKey(sessionAccountId) &&
                    relationships.get(sessionAccountId)[1] == account.getAccountId() &&
                    relationships.get(sessionAccountId)[0] == 1) {
                userPage.addObject("friendStatus", "required");
            }
            for (Map.Entry<Integer, int[]> entry : relationships.entrySet()) {
                if (entry.getValue()[0] > 1) {
                    friends.add(accountService.getAccountById(entry.getKey()));
                }
            }
            userPage.addObject("friendList", friends);
        }
    }

    @GetMapping(value = "/toXML/{accountId}")
    public HttpEntity<byte[]> createXML(@PathVariable int accountId) throws UnsupportedEncodingException {
        logger.info("Received GET request to download account information in XML. Account id = {}", accountId);
        Account account = accountService.getAccountById(accountId);
        account.setPhones(new ArrayList<>(account.getPhones()));
        XStream xStream = new XStream(new DomDriver());
        xStream.processAnnotations(Account.class);
        String xmlDeclaration = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        byte[] bytes = (xmlDeclaration + xStream.toXML(account)).getBytes("UTF-8");
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_XML);
        header.set("Content-Disposition", "attachment; filename=" + account.getAccountId() + ".xml");
        return new HttpEntity<>(bytes, header);
    }

    @PostMapping(value = "/fromXML")
    public ModelAndView readXML(@RequestParam("strXml") String xml) {
        Account sessionAcc = (Account) session.getAttribute("sessionAcc");
        logger.info("Received POST request to update account information with XML. Account id = {}",
                sessionAcc.getAccountId());
        ModelAndView modelAndView = new ModelAndView("editprofile");
        int accountId = sessionAcc.getAccountId();
        Account updatedAccount = accountService.getAccountById(accountId);
        if (xml == null || xml.isEmpty()) {
            logger.info("Update account (id = {}) with XML file. File is empty (or null)", accountId);
            modelAndView.addObject("mainErrMsg", "XML file is empty (or null)!");
            modelAndView.addObject("updatedAccount", updatedAccount);
            return modelAndView;
        }
        try {
            validate(xml);
            XStream xstream = getAccountXStream();
            Account xmlAccount = (Account) xstream.fromXML(xml);
            xmlAccount.setAccountId(accountId);
            xmlAccount.setPassword(sessionAcc.getPassword());
            modelAndView.addObject("account", xmlAccount);
            logger.info("Update account (id = {}) with XML file. Data uploaded", accountId);
            return modelAndView;
        } catch (SAXException | IOException e) {
            logger.info("Update account (id = {}) with XML file. File is invalid", accountId);
            modelAndView.addObject("mainErrMsg", "XML file is invalid");
            modelAndView.addObject("account", updatedAccount);
            return modelAndView;
        }
    }
}
