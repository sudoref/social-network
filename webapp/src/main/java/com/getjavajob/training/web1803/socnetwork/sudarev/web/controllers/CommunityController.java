package com.getjavajob.training.web1803.socnetwork.sudarev.web.controllers;

import com.getjavajob.training.web1803.socnetwork.sudarev.common.Account;
import com.getjavajob.training.web1803.socnetwork.sudarev.common.Group;
import com.getjavajob.training.web1803.socnetwork.sudarev.common.Membership;
import com.getjavajob.training.web1803.socnetwork.sudarev.common.Message;
import com.getjavajob.training.web1803.socnetwork.sudarev.service.AccountService;
import com.getjavajob.training.web1803.socnetwork.sudarev.service.GroupService;
import com.getjavajob.training.web1803.socnetwork.sudarev.service.MessageService;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.*;

import static java.util.Objects.nonNull;

@Controller
public class CommunityController {

    private static final Logger logger = LoggerFactory.getLogger(CommonController.class);
    @Autowired
    private GroupService groupService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private HttpSession session;

    @RequestMapping(value = "/createcommunity")
    public String createCommunity() {
        return "createcommunity";
    }

    @RequestMapping("/handlemembership")
    public String handleMembership(@RequestParam(value = "group", required = false) String groupName,
                                   @RequestParam(value = "membershipReq", required = false) String membershipReq,
                                   @RequestParam(value = "handleStatus", required = false) String handleStatus,
                                   @RequestParam(value = "member", required = false) Integer communityUser) {
        logger.info("handling membership status for group={} and account with id={}", groupName, communityUser);
        Group group = groupService.getGroupByName(groupName);
        if (nonNull(membershipReq)) {
            Account sessionAccount = (Account) session.getAttribute("sessionAcc");
            switch (membershipReq) {
                case "leave community":
                case "remove request": {
                    groupService.removeAccountFromGroup(sessionAccount, group);
                    return "redirect:/account?id=" + sessionAccount.getAccountId();
                }
                case "join community": {
                    groupService.requestAccountForGroupMembership(sessionAccount, group);
                    break;
                }
                default:
                    break;
            }
            return "redirect:/community?name=" + group.getName();
        } else if (nonNull(handleStatus)) {
            Account member = accountService.getAccountById(communityUser);
            switch (handleStatus) {
                case "remove moderator": {
                    groupService.makeAccountAsGroupMembership(member, group);
                    break;
                }
                case "remove member": {
                    groupService.removeAccountFromGroup(member, group);
                    break;
                }
                case "add moderator": {
                    groupService.makeAccountGroupModerator(member, group);
                    break;
                }
                case "accept": {
                    groupService.makeAccountAsGroupMembership(member, group);
                    break;
                }
                case "decline": {
                    groupService.removeAccountFromGroup(member, group);
                    break;
                }
                default:
                    break;
            }
            return "redirect:/members?name=" + group.getName();
        }
        return null;
    }

    @RequestMapping("/members")
    public ModelAndView membersList(@RequestParam(value = "name") String name) {
        logger.info("Get all members and their status for group ={}", name);
        Group group = groupService.getGroupByName(name);
        List<Account> moderators = new ArrayList<>();
        List<Account> required = new ArrayList<>();
        List<Account> members = new ArrayList<>();
        ModelAndView modelAndView = new ModelAndView("members");
        Account sessionAccount = (Account) session.getAttribute("sessionAcc");
        if (groupService.getMembershipStatus(sessionAccount, group) > 2) {
            modelAndView.addObject("isModerator", true);
        }
        for (Membership membership : group.getMembers()) {
            Account account = membership.getAccount();
            switch (groupService.getMembershipStatus(account, group)) {
                case 1: {
                    required.add(account);
                    break;
                }
                case 2: {
                    members.add(account);
                    break;
                }
                case 3: {
                    moderators.add(account);
                    break;
                }
                default:
                    break;
            }
        }
        modelAndView.addObject("name", group.getName());
        modelAndView.addObject("moderators", moderators);
        modelAndView.addObject("required", required);
        modelAndView.addObject("members", members);
        return modelAndView;
    }

    @RequestMapping("/creatingCommunity")
    public String createCommunity(@ModelAttribute Group group,
                                  @RequestParam(value = "photo", required = false) MultipartFile photo) throws IOException {
        logger.info("Creating new Community with name ={}", group.getName());
        Account sessionAcc = (Account) session.getAttribute("sessionAcc");
        group.setCreator(sessionAcc.getEmail());
        if (nonNull(photo) &&
                photo.getContentType().equals("image/jpeg") ||
                photo.getContentType().equals("image/png")) {
            group.setAvatar(IOUtils.toByteArray(new BufferedInputStream((photo.getInputStream()))));
        }
        if (nonNull(groupService.createGroup(group))) {
            return "redirect:/community?name=" + group.getName();
        } else {
            return "redirect:/account?id=" + sessionAcc.getAccountId() + "&communityError=alreadyExists";
        }
    }

    @RequestMapping("/community")
    public ModelAndView communityView(@RequestParam(value = "name") String name) {
        logger.info("Receive GET request to show Community with name={}", name);
        Group group = groupService.getGroupByName(name);
        Account sessionAcc = (Account) session.getAttribute("sessionAcc");
        Account account = accountService.getAccountById(sessionAcc.getAccountId());
        ModelAndView modelAndView = new ModelAndView("community");
        switch (groupService.getMembershipStatus(account, group)) {
            case 3: {
                modelAndView.addObject("role", "moderator");
                break;
            }
            case 2: {
                modelAndView.addObject("role", "member");
                break;
            }
            case 1: {
                modelAndView.addObject("role", "requested");
                break;
            }
            default:
                break;
        }
        List<Message> posts = messageService.getAllGroupMessages(group.getName());
        Collections.reverse(posts);
        Map<Message, Account> postMap = new LinkedHashMap<>();
        for (Message post : posts) {
            if (post.getImage().length <= 0) {
                post.setImage(null);
            }
            Account writer = post.getSender();
            postMap.put(post, writer);
        }
        List<Account> accountList = new ArrayList<>();
        if (group.getMembers().size() > 3) {
            for (int i = 0; i < 3; i++) {
                accountList.add(i, accountService.getAccountByEmail(group.getMembers().iterator().next().getAccount().getEmail()));
            }
        } else {
            for (Membership mem : group.getMembers()) {
                accountList.add(mem.getAccount());
            }
        }
        modelAndView.addObject("group", group);
        modelAndView.addObject("members", accountList);
        modelAndView.addObject("messages", postMap);
        return modelAndView;
    }
}
