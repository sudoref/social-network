package com.getjavajob.training.web1803.socnetwork.sudarev.web.controllers;

import com.getjavajob.training.web1803.socnetwork.sudarev.common.Account;
import com.getjavajob.training.web1803.socnetwork.sudarev.common.Group;
import com.getjavajob.training.web1803.socnetwork.sudarev.common.Message;
import com.getjavajob.training.web1803.socnetwork.sudarev.service.AccountService;
import com.getjavajob.training.web1803.socnetwork.sudarev.service.GroupService;
import com.getjavajob.training.web1803.socnetwork.sudarev.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.nonNull;

@Controller
public class CommonController {

    private static final Logger logger = LoggerFactory.getLogger(CommonController.class);
    @Autowired
    private MessageService messageService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private AccountService accountService;

    @RequestMapping(value = {"/", "/index", "/login"})
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/registration")
    public String registration() {
        return "registration";
    }

    @RequestMapping(value = "/printAvatar")
    public void printAvatar(@RequestParam(value = "accountId", required = false) int accountID,
                            HttpServletResponse resp) throws IOException {
        Account account = accountService.getAccountById(accountID);
        if (nonNull(account.getAvatar())) {
            resp.getOutputStream().write(account.getAvatar());
        } else {
            resp.sendRedirect("https://www.troostwijk.nl/static/images/admin/avatar.png");
        }
    }

    @RequestMapping(value = "/printMessage")
    public void printImage(@RequestParam(value = "messageId", required = false) Integer messageID,
                           HttpServletResponse resp) throws IOException {
        Message message = messageService.getMessageByKey(messageID);
        if (nonNull(message.getImage())) {
            resp.getOutputStream().write(message.getImage());
        }
    }

    @RequestMapping(value = "/printGroup")
    public void printGroup(@RequestParam(value = "groupId", required = false) String groupID,
                           HttpServletResponse resp) throws IOException {
        if (nonNull(groupID)) {
            Group group = groupService.getGroupByName(groupID);
            if (nonNull(group.getAvatar())) {
                resp.getOutputStream().write(group.getAvatar());
            } else {
                resp.sendRedirect("http://www.hyundai-vip.ru/media/hpromise/no-photo.png");
            }
        }
    }

    @GetMapping("/searchCommunity")
    public ModelAndView searchGroup(@RequestParam String search) {
        logger.info("searching group with pattern = {}", search);
        ModelAndView modelSearch = new ModelAndView("searchGroup");
        Integer qty = groupService.getGroupQty(search);
        modelSearch.addObject("searchPattern", search);
        modelSearch.addObject("objectQty", qty);
        return modelSearch;
    }

    @GetMapping("/search")
    public ModelAndView search(@RequestParam String search) {
        logger.info("searching account with pattern = {}", search);
        ModelAndView modelSearch = new ModelAndView("searchResult");
        Integer qty = accountService.getAccountQty(search);
        modelSearch.addObject("searchPattern", search);
        modelSearch.addObject("objectQty", qty);
        return modelSearch;
    }

    @RequestMapping(value = "/getSearch")
    @ResponseBody
    public List getAccounts(@RequestParam(value = "search") String search) {
        List result = new ArrayList();
        if (nonNull(search)) {
            List<Account> accounts = accountService.searchAccounts(search);
            List<Group> groups = groupService.searchGroups(search);
            result.addAll(accounts);
            result.addAll(groups);
        }
        return result;
    }

    @GetMapping("/ajaxPaginationSearch")
    @ResponseBody
    public List autocomplete(@RequestParam(value = "searchPattern") String searchPattern,
                             @RequestParam("page") int page,
                             @RequestParam(value = "maxResult") int maxResult) {
        return accountService.searchByPage(searchPattern, page, maxResult);
    }

    @GetMapping("/ajaxPaginationGroupSearch")
    @ResponseBody
    public List autocompleteGroup(@RequestParam(value = "searchPattern") String searchPattern,
                                  @RequestParam("page") int page,
                                  @RequestParam(value = "maxResult") int maxResult) {
        List<Group> groups = groupService.searchByPages(searchPattern, page, maxResult);
        for (Group group : groups) {
            group.setMembers(null);
            group.setAvatar(null);
        }
        return groups;
    }
}
