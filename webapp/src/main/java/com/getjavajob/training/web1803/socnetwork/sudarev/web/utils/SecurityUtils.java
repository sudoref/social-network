package com.getjavajob.training.web1803.socnetwork.sudarev.web.utils;

import com.getjavajob.training.web1803.socnetwork.sudarev.common.Account;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public final class SecurityUtils {

    private SecurityUtils() {
    }

    public static Account getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth instanceof AnonymousAuthenticationToken ? null : (Account) auth.getPrincipal();
    }
}