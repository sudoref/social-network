package com.getjavajob.training.web1803.socnetwork.sudarev.web.controllers;

import com.getjavajob.training.web1803.socnetwork.sudarev.common.Account;
import com.getjavajob.training.web1803.socnetwork.sudarev.common.Message;
import com.getjavajob.training.web1803.socnetwork.sudarev.service.AccountService;
import com.getjavajob.training.web1803.socnetwork.sudarev.service.MessageService;
import com.getjavajob.training.web1803.socnetwork.sudarev.service.RelationshipService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Objects.nonNull;
import static org.apache.commons.io.IOUtils.toByteArray;

@Controller
public class MessageController {

    private static final Logger logger = LoggerFactory.getLogger(MessageController.class);
    @Autowired
    private HttpSession session;
    @Autowired
    private RelationshipService relationshipService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private AccountService accountService;

    @RequestMapping("/conversationList")
    public ModelAndView conversationList() {

        ModelAndView conversationsModel = new ModelAndView("conversationlist");
        Account account = (Account) session.getAttribute("sessionAcc");
        logger.info("getting conversation list for account {}",account.getEmail());
        List<List<Message>> conversations = new ArrayList<>();
        List<Integer> friends = relationshipService.getFriendList(account);
        for (Integer accountID : friends) {
            if (messageService.receivePrivateConversation(accountID, account.getAccountId()).size() > 0) {
                conversations.add(messageService.receivePrivateConversation(accountID, account.getAccountId()));
            }
        }
        Map<Account, String> talks = new HashMap<>();
        for (List<Message> list : conversations) {
            Message lastMessage = list.get(list.size() - 1);
            Account lastWriter;
            if (!lastMessage.getSender().getAccountId().equals(account.getAccountId())) {
                lastWriter = accountService.getAccountById(lastMessage.getSender().getAccountId());
            } else {
                lastWriter = accountService.getAccountById(lastMessage.getRecipient());
            }
            talks.put(lastWriter, lastMessage.getText());
        }
        conversationsModel.addObject("conversations", talks);
        return conversationsModel;
    }

    @RequestMapping(value = "/messenger")
    public String registration(Model model) {
        Account account = (Account) session.getAttribute("sessionAcc");
        model.addAttribute("username", account.getFirstName());
        return "messenger";
    }

    @RequestMapping("/privateConversation")
    public ModelAndView privateConversation(@RequestParam(value = "id", required = false) int friendID) {
        ModelAndView privateModel = new ModelAndView("conversation");
        Account account = (Account) session.getAttribute("sessionAcc");
        Account friend = accountService.getAccountById(friendID);
        List<Message> conversation = messageService.receivePrivateConversation(friendID, account.getAccountId());
        if (nonNull(conversation)) {
            for (Message message : conversation) {
                if (message.getImage().length == 0) {
                    message.setImage(null);
                }
            }
            privateModel.addObject("friend", friend);
            privateModel.addObject("friendID", friend.getAccountId());
            privateModel.addObject("accountID", account.getAccountId());
            privateModel.addObject("conversation", conversation);
            return privateModel;
        } else {
            return null;
        }
    }

    @RequestMapping("/sendMessage")
    public ModelAndView sendMessage(@RequestParam(value = "friendID", required = false) int friendID,
                                    @RequestParam(value = "message", required = false) String textMessage,
                                    @RequestParam(value = "photo", required = false) MultipartFile photo) throws IOException {
        Account account = (Account) session.getAttribute("sessionAcc");
        logger.info("account {} sending message",account.getEmail());
        Account friend = accountService.getAccountById(friendID);
        Message message = new Message();
        message.setSender(account);
        message.setRecipient(friend.getAccountId());
        message.setType(Message.Type.valueOf("PRIVATE"));
        message.setRegistrationDate(LocalDateTime.now());
        message.setText(textMessage);
        if (nonNull(photo)) {
            message.setImage(toByteArray(new BufferedInputStream((photo.getInputStream()))));
        } else {
            message.setImage(null);
        }
        if (nonNull(messageService.sendPrivateMessage(message))) {
            ModelAndView sendModel = new ModelAndView("redirect:/privateConversation");
            sendModel.addObject("id", friendID);
            return sendModel;
        } else {
            return new ModelAndView("redirect:/account");
        }
    }

    @RequestMapping("/postMessages")
    public String postMessage(@RequestParam(value = "id", required = false) String id,
                              @RequestParam(value = "newMessage", required = false) String newMessage,
                              @RequestParam(value = "groupID", required = false) String groupID,
                              @RequestParam(value = "messageIDToRemove", required = false) Integer messageIDToRemove,
                              @RequestParam(value = "photo", required = false) MultipartFile photo) throws IOException {
        Account account = (Account) session.getAttribute("sessionAcc");
        if (nonNull(newMessage) && nonNull(groupID)) {
            addNewPost(account, newMessage, groupID, id, "GROUP", photo);
        } else if (nonNull(newMessage)) {
            addNewPost(account, newMessage, id, id, "POST", photo);
        } else if (nonNull(messageIDToRemove)) {
            removePost(messageIDToRemove);
        }
        if (nonNull(groupID)) {
            logger.info("account {} is posting message in community {}",account.getEmail(), groupID);
            return "redirect:/community?name=" + id;
        } else {
            logger.info("account {} is posting message to wall",account.getEmail());
            return "redirect:/account?id=" + id;
        }
    }

    private void addNewPost(Account account, String messageText, String recipient, String conversationName, String type, MultipartFile photo) throws IOException {
        Message message = new Message();
        message.setSender(account);
        message.setRecipient(Integer.valueOf(recipient));
        message.setConversation(conversationName);
        message.setType(Message.Type.valueOf(type));
        message.setRegistrationDate(LocalDateTime.now());
        message.setText(messageText);
        if (nonNull(photo)) {
            message.setImage(toByteArray(new BufferedInputStream((photo.getInputStream()))));
        }
        messageService.sendPostMessage(message);
    }

    private void removePost(int messageID) {
        Message message = messageService.getMessageByKey(messageID);
        if (nonNull(message)) {
            messageService.deletePostFromAccount(message);
        }
    }
}

