package com.getjavajob.training.web1803.socnetwork.sudarev.web.controllers;

import com.getjavajob.training.web1803.socnetwork.sudarev.common.Account;
import com.getjavajob.training.web1803.socnetwork.sudarev.common.Relationship;
import com.getjavajob.training.web1803.socnetwork.sudarev.service.AccountService;
import com.getjavajob.training.web1803.socnetwork.sudarev.service.RelationshipService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.nonNull;

@Controller
public class RelationshipController {

    private static final Logger logger = LoggerFactory.getLogger(RelationshipController.class);
    @Autowired
    private AccountService accountService;
    @Autowired
    private volatile HttpSession session;
    @Autowired
    private RelationshipService relationshipService;

    @RequestMapping("/friendList")
    public ModelAndView friendList(@RequestParam("id") int id) {

        ModelAndView modelFriendList = new ModelAndView("friendlist");
        Account account = accountService.getAccountById(id);
        logger.info("getting friend list for account {}",account.getEmail());
        Map<Account, String> relationship = new HashMap<>();
        account.setRelationships(relationshipService.getAllRelationships(account));
        if (nonNull(account.getRelationships())) {
            for (Map.Entry<Integer, int[]> entry : account.getRelationships().entrySet()) {
                if (entry.getValue()[0] == 1 && entry.getValue()[1] != id) {
                    Account friend = accountService.getAccountById(entry.getKey());
                    relationship.put(friend, "possibleFriend");
                } else if (entry.getValue()[0] > 1) {
                    Account friend = accountService.getAccountById(entry.getKey());
                    relationship.put(friend, "friend");
                } else if (entry.getValue()[0] == 1 && entry.getValue()[1] == id) {
                    Account friend = accountService.getAccountById(entry.getKey());
                    relationship.put(friend, "requestedFriend");
                }
            }
        }
        modelFriendList.addObject("relationship", relationship);
        return modelFriendList;
    }

    @RequestMapping("/accept")
    public String handleFriendRequest(@RequestParam(value = "accept", required = false) Integer accept,
                                      @RequestParam(value = "deny", required = false) Integer deny) {
        Account initiator = (Account) session.getAttribute("sessionAcc");
        if (nonNull(accept)) {
            logger.info("account {} accept friend request", initiator.getEmail());
            Account friend = accountService.getAccountById(accept);
            relationshipService.handleStatus(initiator, friend, 2);
        } else if (nonNull(deny)) {
            logger.info("account {} denied friend request", initiator.getEmail());
            Account friend = accountService.getAccountById(deny);
            relationshipService.removeFromFriends(initiator, friend);
        }
        return "redirect:/account?id=" + initiator.getAccountId();
    }

    @RequestMapping("/relationshipReq")
    public String relationshipRequest(@RequestParam(value = "friendReq", required = false) Integer friendReq,
                                      @RequestParam(value = "removeRequest", required = false) Integer removeRequest,
                                      @RequestParam(value = "accept", required = false) Integer accept,
                                      @RequestParam(value = "deleteFriend", required = false) Integer deleteFriend) {

        Account account = (Account) session.getAttribute("sessionAcc");
        if (nonNull(friendReq)) {
            Account friend = accountService.getAccountById(friendReq);
            Relationship relationship = relationshipService.friendRequest(account, friend);
            if (nonNull(relationship)) {
                session.setAttribute("relationships", account.getRelationships());
                return "redirect:/account?id=" + friendReq;
            }
        } else if (nonNull(removeRequest)) {
            Account friend = accountService.getAccountById(removeRequest);
            relationshipService.removeFromFriends(account, friend);
            if (nonNull(account)) {
                session.setAttribute("relationships", account.getRelationships());
                return "redirect:/account?id=" + account.getAccountId();
            }
        } else if (nonNull(accept) && nonNull(account)) {
            Account friend = accountService.getAccountById(accept);
            relationshipService.handleStatus(account, friend, 2);
            session.setAttribute("relationships", account.getRelationships());
            return "redirect:/account?id=" + account.getAccountId();
        } else {
            Account friend = accountService.getAccountById(deleteFriend);
            relationshipService.removeFromFriends(account, friend);
            if (nonNull(account)) {
                session.setAttribute("relationships", account.getRelationships());
                return "redirect:/account?id=" + account.getAccountId();
            }
        }
        return null;
    }
}
