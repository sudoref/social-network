package com.getjavajob.training.web1803.socnetwork.sudarev.web.config;

import com.getjavajob.training.web1803.socnetwork.sudarev.common.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.getjavajob.training.web1803.socnetwork.sudarev.web.utils.SecurityUtils.getCurrentUser;

@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Autowired
    HttpSession session;

    public CustomAuthenticationSuccessHandler() {
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        Account account = getCurrentUser();
        session.setAttribute("sessionAcc", account);
        response.setStatus(HttpServletResponse.SC_OK);
        if (account != null) {
            response.sendRedirect("/account?id=" + account.getAccountId());
        }
    }
}
