function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validate() {
    var email = document.registrationForm.email.value;
    var passlength = document.registrationForm.password;
    if (!validateEmail(email)) {
        alert("bad format email");
        document.registrationForm.email.focus();
        return false;
    }

    if (passlength.value.length < 5) {
        alert("Password is too short");
        document.registrationForm.password.focus();
        return false;
    }
}