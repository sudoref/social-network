$(document).ready(function () {
    let inputFile = document.getElementById("xml-btn");
    inputFile.addEventListener("change", function (event) {
        let file = event.target.files[0];
        if (!file.type.match("text/xml*")) {
            createMessage("Unexpected format!");
            return;
        }
        let reader = new FileReader();
        reader.onload = function (event) {
            let str = event.target.result;
            submit(context + "/fromXML", "strXml", str);
        };
        reader.onerror = function (event) {
            createMessage("Read error: " + event.target.error.code);
        };
        reader.readAsText(file);
    });

    function createMessage(text) {
        let msg = $('#upload-xml-file-info');
        msg.text(text);
    }

    function submit(url, name, value) {
        let form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", url);
        let input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", name);
        input.setAttribute("value", value);
        form.appendChild(input);
        document.body.appendChild(form);
        form.submit();
    }
});