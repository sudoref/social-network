$(document).ready(function () {
    $("#someText").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: context + "/getSearch?",
                data: {
                    search: request.term
                },
                success: function (data) {
                    response($.map(data, function (item, i) {
                        if (i > 4) {
                            return null;
                        }
                        if (item.name != null) {
                            return {
                                group: item.name,
                                url: context + "/community?name=" + item.name,
                                value: "community: " + item.name
                            }
                        } else {
                            return {
                                group: item.name,
                                url: context + "/account?id=" + item.accountId,
                                value: "account: " + item.firstName + ' ' + item.surname
                            }
                        }
                    }))
                }
            })
        },
        select: function (event, ui) {
            window.location.href = ui.item.url;
        },
        minLength: 3
    });
});