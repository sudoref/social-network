function confirm() {

    var confirmdialog = $('<div></div>').appendTo('body')
        .html('<div><h6>Are you sure want to save the changes?!</h6></div>')
        .dialog({
            modal: true, zIndex: 10000, autoOpen: false,
            width: 'auto', resizable: false,
            buttons: {
                Yes: function () {
                    $(this).dialog("close");
                    document.getElementById("saveForm").submit();
                },
                No: function () {
                    $(this).dialog("close");
                    return false;
                }
            },
            close: function (event, ui) {
                $(this).remove();
                return false;
            }
        });

    return confirmdialog.dialog("open");
}