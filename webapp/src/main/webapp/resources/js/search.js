let searchResult = $('#searchResult');
let pattern = $('#searchPattern').val();
let qty = $('#objectQty').val();
let rowsOnPage = 4;
let pages = Math.ceil(qty / rowsOnPage);

$(document).ready(function () {
    $(function () {
        if (pages > 1) {
            paginationSearch();
        } else {
            ajaxSearch(0);
        }
    });

    function paginationSearch() {
        $('#nsw-pagination').twbsPagination({
            totalPages: pages,
            visiblePages: 4,
            onPageClick: function (event, page) {
                $('html, body').animate({scrollTop: $("#header").offset().top}, 'slow');
                ajaxSearch(page - 1);
            }
        });
    }

    function ajaxSearch(page) {
        $.get(context + '/ajaxPaginationSearch', {searchPattern: pattern, page: page, maxResult: rowsOnPage},
            function (data) {
                searchResult.empty();
                if (!data.length) {
                    searchResult
                        .append('<p class="nsw-tnr centered"> Nothing found...</p>')
                } else {
                    $.each(data, function (index, item) {
                        createAccountRow(item);
                    });
                }
            })
    }

    function createAccountRow(account) {
        $('#searchResult')
            .append('<div class="card bg-light">' +
                '                    <div class="card-body">' +
                '                        <div class="row">' +
                '                           <div class="col-md-9 my-auto"> ' +
                '                                <a href="' + context + '/account?id=' + account.accountID + '" class="nsw-tnr">' +
                '                                        <h5>' + account.firstName + ' ' + account.surname + '</h5></a>' +
                '                            </div>' +
                '                        </div>' +
                '                    </div>' +
                '                </div>' +
                '                <br/>')
    }
});