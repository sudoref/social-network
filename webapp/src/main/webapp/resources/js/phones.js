//define counter
let cloneIndex = $(".clone").length;
//add new section
$('body').on('click', '.addsection', function () {
    var div = document.createElement('div');
    div.className = 'clone';
    div.innerHTML =
        "<input id='phones" + cloneIndex + ".number' name='phones[" + cloneIndex + "].number' type='text' value=''/>\n" +
        "<select id=\"phones" + cloneIndex + ".type\" name=\"phones[" + cloneIndex + "].type\">\n" +
        "<option value=\"\" selected=\"selected\">--number--</option>\n" +
        "<option value=\"HOME\">home phone</option>\n" +
        "<option value=\"MOB\">mobile phone</option>\n" +
        "<option value=\"WORK\">work phone</option>\n" +
        "</select>\n" +
        "<a href=\"#\" class='remove'>Remove phone</a>";
    document.getElementById('sections').appendChild(div);
    cloneIndex++;
    return false;
});
//remove section
$('#sections').on('click', '.remove', function () {
    //fade out section
    $(this).fadeOut(300, function () {
        //remove parent element (main section)
        $(this).parent().empty();
        return false;
    });
    return false;
});
