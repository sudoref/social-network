<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>
<head>
    <title>${sessionScope.sessionAcc.getFirstName()}
    </title>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        html, body, h1, h2, h3, h4, h5, h6 {
            font-family: "Roboto", sans-serif
        }

        <%@include file="../../resources/css/accountstyle.css"%>
        <%@include file="../../resources/css/curves.css"%>
        <%@include file="../../resources/css/header.css"%>
        <%@include file="../../resources/css/chat.css"%>

    </style>
    <script>let context = "${pageContext.request.contextPath}"</script>

</head>

<body class="w3-light-grey">

<nav class="nav navbar-fixed-top w3-teal navbar-principal"
     style="min-height: 55px;">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                    aria-controls="navbar">
            </button>
            <a class="navbar-brand" href="index"><b>My Network</b></a>
        </div>

        <div id="navbar">
            <div class="col-md-5 col-sm-4">
                <div class="navbar-form">
                    <form method="get" action="search">
                        <div class="form-group" style="display: inline;">
                            <div class="input-group" style="display: table;">
                                <input class="form-control" id="someText" name="search" placeholder="Search..."
                                       type="text">
                                <span class="input-group-addon" style="width: 10%;">
                                <span class="glyphicon glyphicon-search">
                                </span>
								</span>
                            </div>
                            <div id="auto-row">
                                <table id="complete-table" class="table table-bordered"
                                       style="position: absolute;  background-color: white; width:350px"></table>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="account?id=${sessionScope.sessionAcc.accountId}">
                    Home
                </a>
                </li>
                <li><a href="${pageContext.request.contextPath}/messenger">
                    Messenger
                </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" role="button">
                        <i class="fa fa-user"></i>
                        <c:out value="${sessionScope.sessionAcc.firstName}"/>
                        <c:if test="${sessionScope.sessionAcc.isAdmin() eq 'true'}">(admin)
                        </c:if><span class="caret"></span></a>
                    <div class="dropdown-content">
                        <a href="conversationList">Messages</a>
                        <a href="${pageContext.request.contextPath}/friendList?id=${sessionScope.sessionAcc.accountId}">Friends</a>
                        <%--<li><a href="ChangePassword">Change Password</a></li>--%>
                    </div>
                </li>
                <li><a href="logout" class="nav-controller"><i class="fa fa-sign-out"></i> Logout</a></li>
            </ul>
        </div>
    </div>
</nav>