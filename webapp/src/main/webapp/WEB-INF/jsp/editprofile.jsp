<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="../template/header.jsp"/>
<style>
    .image-upload > input {
        display: none;
    }

    .image-upload img {
        width: 100px;
        height: 100px;
        cursor: pointer;
    }
</style>
<script src="//code.jquery.com/jquery-3.3.1.js"></script>
<div class="w3-container w3-card w3-white" style="max-width:800px; margin: 4% auto auto;">
    <div class="col-sm-12 col-sm-offset-0">
        <form id="saveForm" method="post" action="editProfile" enctype="multipart/form-data">
            <div class="card wizard-card ct-wizard-orange" id="wizard">
                <div class="col-sm-4 col-sm-offset-1">
                    <div class="picture-container">
                        <div class="image-upload" style="margin-top: 50px">
                            <label for="file-input">
                                <img src="${pageContext.request.contextPath}/printAvatar?accountId=${account.accountId}"
                                     style="width: 50%; height: 50%">
                            </label>
                            <input id="file-input" name="photo" type="file"/>
                        </div>
                        <h6>Choose Picture</h6>
                    </div>
                </div>

                <div class="col-sm-6 ">
                    <div class="from-xml-btn">
                        <span class="label" id="upload-xml-file-info"></span>
                        <br>
                        <label class="w3-button " for="xml-btn">
                            <input id="xml-btn" name="xml" type="file" style="opacity: 0">
                            From XML
                        </label>
                    </div>
                    <div class="form-group" style="width: 100%; margin-top: 10%">
                        <label>First Name</label>
                        <input type="text" class="form-control from-reg" name="firstName"
                               value="${account.firstName}" autofocus="autofocus">
                        <input type="text" name="accountID" hidden value="${account.accountId}">
                        <input type="text" name="password" hidden value="${account.password}">

                    </div>
                    <div class="form-group">
                        <label>Surname</label>
                        <input type="text" class="form-control from-reg" name="surname"
                               value="${account.surname}">

                    </div>
                    <div class="form-group">
                        <label>Second Name</label>
                        <input type="text" class="form-control from-reg" name="midName"
                               value="${account.midName}">

                    </div>
                </div>

                <div class="col-sm-10 col-sm-offset-1">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control from-reg" name="email"
                               value="${account.email}">
                    </div>
                </div>
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="form-group">
                        <label>Skype</label>
                        <input type="text" class="form-control from-reg" name="skype"
                               value="${account.skype}">
                    </div>
                </div>
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="form-group">
                        <label>icq</label>
                        <input type="text" class="form-control from-reg" name="icq"
                               value="${account.icq}">
                    </div>
                </div>

                <div class="col-sm-10 col-sm-offset-1">
                    <div class="form-group">
                        <label>home address</label>
                        <input type="text" class="form-control from-reg" name="homeAddress"
                               value="${account.homeAddress}">
                    </div>
                </div>

                <div class="col-sm-10 col-sm-offset-1">
                    <div class="form-group">
                        <label>work address</label>
                        <input type="text" class="form-control from-reg" name="workAddress"
                               value="${account.workAddress}">
                    </div>
                </div>
                <div id="sections" class="col-sm-10 col-sm-offset-1">
                    <c:forEach items="${account.phones}" varStatus="status">
                        <div class="clone">
                            <form:input path="account.phones[${status.index}].number"/>
                            <form:select path="account.phones[${status.index}].type">
                                <form:option value="">--number--</form:option>
                                <form:option value="HOME">home phone</form:option>
                                <form:option value="MOB">mobile phone</form:option>
                                <form:option value="WORK">work phone</form:option>
                            </form:select>
                            <a href="#" class='remove'>Remove phone</a>
                        </div>
                    </c:forEach>
                </div>

                <div class="col-sm-10 col-sm-offset-1">
                    <i class="fa fa-phone fa-fw w3-margin-right w3-large w3-text-teal"></i>
                    <a href="#" class='addsection'>Add phone</a></div>
                <br>
                <br>
                <div class="col-sm-5 col-sm-offset-1">
                    <div class="form-group">
                        <label>Date of Birth
                            <input type="date" class="form-control from-reg" name="birthDay"
                                   value="${account.birthDate}">
                        </label>
                    </div>
                </div>

                <div class="col-sm-5 col-sm-offset-1">
                    <div class="form-group">
                        <label>Gender:</label><br>
                        Male <input type="radio" name="gender" value="Male"
                    ${account.gender == 'Male'? 'checked':''}>
                        Female <input type="radio" name="gender" value="Female"
                    ${account.gender == 'Female'? 'checked':''}>
                    </div>
                </div>
                <br>
                <div id="submit" class="wizard-footer text-center ">
                    <br>
                    <input type="button" name="btnSubmit" onclick="confirm()" value="Save" class="w3-button "
                           style="width: 30%; background-color: #467ed9; color: #ffffff; ">

                </div>
                <div class="wizard-footer text-left" style="margin-left: 10%; position: absolute">
                    <c:if test="${sessionScope.sessionAcc.isAdmin()}">
                        <label></label>
                        <c:choose>
                            <c:when test="${account.isAdmin()}">
                                <input type="checkbox" name="isAdmin" checked="checked">
                            </c:when>
                            <c:otherwise>
                                <input type="checkbox" name="isAdmin">
                            </c:otherwise>
                        </c:choose>
                        admin rights
                    </c:if>
                </div>
                <br>
            </div>
        </form>
        <form method="post" action="deleteProfile">
            <input type="submit" name="delete" value="Delete profile" class="w3-button"
                   style="width: 30%; margin-left: 65%; background-color: #467ed9; color: #ffffff;">
            <input type="hidden" name="accountID" value="${account.accountId}">
        </form>
        <br>

    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>let context = "${pageContext.request.contextPath}"</script>
<script>
    <%@include file="../../resources/js/phones.js"%>
    <%@include file="../../resources/js/confirm.js"%>
</script>
<script>
    $(document).ready(function () {
        let inputFile = document.getElementById("xml-btn");
        inputFile.addEventListener("change", function (event) {
            let file = event.target.files[0];
            if (!file.type.match("text/xml*")) {
                createMessage("Unexpected format!");
                return;
            }
            let reader = new FileReader();
            reader.onload = function (event) {
                let str = event.target.result;
                submit(context + "/fromXML", "strXml", str);
            };
            reader.onerror = function (event) {
                createMessage("Read error: " + event.target.error.code);
            };
            reader.readAsText(file);
        });

        function createMessage(text) {

            let msg = $('#upload-xml-file-info');
            msg.text(text);
        }

        function submit(url, name, value) {
            alert(url);
            let form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("action", url);
            let input = document.createElement("input");
            input.setAttribute("type", "hidden");
            input.setAttribute("name", name);
            input.setAttribute("value", value);
            form.appendChild(input);
            document.body.appendChild(form);
            form.submit();
        }
    });
</script>

<jsp:include page="../template/footer.jsp"/>