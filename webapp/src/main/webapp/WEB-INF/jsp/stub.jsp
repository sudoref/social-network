<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../template/header.jsp"/>
<body class="w3-light-grey">

<!-- Page Container -->
<div class="w3-content w3-margin-top" style="max-width:1400px;">
    <!-- The Grid -->
    <div class="w3-row-padding">
        <!-- Left Column -->
        <div class="w3-third">

            <div class="w3-white w3-text-grey w3-card-4 " style="width:500px; text-align: center">
                <div class="w3-display-container">
                    <img src="https://www.troostwijk.nl/static/images/admin/avatar.png" style="width:100%" alt="Avatar">
                    <h1> PAGE IS NOT EXISTS</h1>
                </div>
            </div>
            <br>
            <!-- End Left Column -->
        </div>
    </div>

    <!-- End Page Container -->
</div>
<jsp:include page="../template/footer.jsp"/>