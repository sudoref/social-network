<%@ page errorPage="error.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<canvas></canvas>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        <%@include file="/resources/css/registration.css"%>
    </style>
    <script src="https://code.jquery.com/jquery-1.12.4.js"
            integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="
            crossorigin="anonymous">
    </script>
    <script>
        <%@include file="/resources/js/validator.js"%>
    </script>
</head>
<body>

<div class="login-box">
    <img src="https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png" class="avatar">
    <h1>Registration</h1>
    <form name="registrationForm" action="registrationNewUser" method="post">
        <c:if test="${not empty errorMessage}">
            <p><span style="color: #ffffff;">
                <c:out value="${errorMessage}"/></span></p>
        </c:if>
        <input type="text" name="email" required="required" placeholder="Enter E-Mail">
        <input type="password" name="password" required="required" placeholder="Enter Password">
        <input type="text" name="firstName" required="required" placeholder="Enter name">
        <input type="text" name="surname" placeholder="Enter surname">
        <input type="text" name="midName" placeholder="Enter second name">
        <input type="text" name="icq" placeholder="Enter icq">
        <input type="text" name="skype" placeholder="Enter skype">
        <p class="fa fa-phone"></p>Phones : <br>
        <input type="text" name="phone" placeholder="Enter phone number" style="width: 70%">
        <select name="type">
            <option value="">--Phone--</option>
            <option value="HOME">
                home phone
            </option>
            <option value="MOB">
                mobile
            </option>
            <option value="WORK">
                office phone
            </option>
        </select>
        <br>
        gender:
        <input type="radio" name="gender" value="Male" style="width: 3%"> M
        <input type="radio" name="gender" value="Female" style="width: 3%"> F
        <label style="margin-left: 20%"> Date of birth:
            <input type="date" name="birthDay" style="width: 27% ">
        </label>
        <br>
        <input type="submit" name="submit" onclick="return validate();" value="Register">
    </form>
</div>

</body>
</html>