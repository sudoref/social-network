<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>
<canvas></canvas>
<head>
    <link rel="stylesheet" href="../../resources/css/curves.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"
            integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="
            crossorigin="anonymous">
    </script>

</head>
<body>

<div class="login-box">
    <img src="${pageContext.request.contextPath}/images/avatar.png" class="avatar" />
    <h1>Login Here</h1>

    <form action="${pageContext.request.contextPath}/login" method="post">
        <c:if test="${param.error}">
            <p>wrong user or password </p>
        </c:if>
        <p>E-Mail</p>
        <input type="text" name="username" placeholder="Enter E-Mail">
        <p>Password</p>
        <input type="password" name="password" placeholder="Enter Password">
        <div>
            <label for="remember"><input name="remember-me" id="remember" type="checkbox" style=" position: absolute">
                <ins style="margin-left: 66%">Remember</ins>
            </label>
        </div>
        <br>
        <input type="submit" name="submit" value="submit">
        Are you new one?
        <a href="${pageContext.request.contextPath}/registration">Register</a>
    </form>
</div>
</body>
</html>
