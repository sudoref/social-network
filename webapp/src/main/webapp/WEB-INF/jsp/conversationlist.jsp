<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="/WEB-INF/template/header.jsp"/>
<div class="w3-container w3-card w3-white" style="width:800px; margin: 4% auto">
    <h4 class="w3-text-grey w3-padding-13"><i
            class="fa fa-envelope fa-fw w3-margin-right w3-large w3-text-teal"></i> My Conversations</h4>
    <hr>
    <c:if test="${empty conversations}">
        Messages not found
    </c:if>
    <c:forEach items="${conversations}" var="conversation">
        <a href="${getContextPath}/privateConversation?id=${conversation.key.getAccountId()}">
            <h4 class="w3-text-grey w3-padding-8">
                <i class="fa fa-user fa-fw w3-margin-right w3-text-teal"></i>${conversation.key.getFirstName()} ${conversation.key.getSurname()}
            </h4><i style="margin-left: 80%">Message: <c:out value="${conversation.value}"/></i>
        </a>
        <hr>
    </c:forEach>
</div>
<jsp:include page="/WEB-INF/template/footer.jsp"/>