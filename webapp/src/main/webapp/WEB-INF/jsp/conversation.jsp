<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>

<script>
    window.onload = function () {
        var objDiv = document.getElementById("chatcontent");
        objDiv.scrollTop = objDiv.scrollHeight;
    };
</script>

<jsp:include page="/WEB-INF/template/header.jsp"/>
<style>
    <%@include file="/resources/css/chat.css"%>
</style>
<div class="w3-container w3-card w3-white" style="width:800px; margin: 4% auto;">
    <div id="chatcontent" class="w3-container w3-card w3-white " style="overflow-y: scroll; height: 600px; margin: 1%">
        <c:forEach items="${conversation}" var="message">
            <c:choose>
                <c:when test="${message.getSender().getAccountId().equals(friend.getAccountId())}">
                    <h5 class="w3-text-grey w3-padding-6" style="margin-bottom: 0">
                        <a href="${pageContext.request.contextPath}/account?id=${friend.getAccountId()}">
                            <img src="https://www.troostwijk.nl/static/images/admin/avatar.png" alt="Avatar"
                                 style="height: 8%; width: 8%">
                                ${friend.getFirstName()}
                        </a>
                    </h5>
                    <i style="margin-left: 10%">${message.getText()}</i>
                    <c:if test="${not empty message.getImage()}">
                        <img src="${pageContext.request.contextPath}/printMessage?messageId=${message.getId()}"
                             height="150px" width="150px" alt="ProfilePic"/>
                    </c:if>
                    <javatime:parseLocalDateTime value="${message.getRegistrationDate()}"
                                                 pattern="yyyy-MM-dd'T'HH:mm:ss" var="parsedDate"/>
                    <span class="time-right"> <javatime:format value="${parsedDate}"
                                                               pattern="HH:mm:ss dd-MM-yyyy"/></span>
                </c:when>
                <c:otherwise>
                    <h5 class="w3-text-grey w3-padding-6" style="margin-bottom: 0">
                        <a href="${pageContext.request.contextPath}/account?id=${sessionScope.accountId}">
                            <img src="https://www.troostwijk.nl/static/images/admin/avatar.png" alt="Avatar"
                                 style="height: 8%; width: 8%">
                                ${sessionScope.firstName}
                        </a>
                    </h5>
                    <i style="margin-left: 10%">${message.getText()}</i>
                    <c:if test="${not empty message.getImage()}">
                        <img src="${pageContext.request.contextPath}/printMessage?messageId=${message.getId()}"
                             height="150px" width="150px" alt="ProfilePic"/>
                    </c:if>
                    <javatime:parseLocalDateTime value="${message.getRegistrationDate()}"
                                                 pattern="yyyy-MM-dd'T'HH:mm:ss" var="parsedDate"/>
                    <span class="time-right"> <javatime:format value="${parsedDate}"
                                                               pattern="HH:mm:ss dd-MM-yyyy"/></span>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </div>
    <form id="form1" method="post" action="sendMessage" enctype="multipart/form-data">
        <div style=" display: inline-block;
                 position:relative;">
            <textarea name="message" maxlength="30" cols="70" rows="3" style="resize: none; margin-left: 1%"></textarea>
            <label for="file-input" style="position:absolute;
                                        bottom:10px;
                                        right:10px;">
                <i class="fa fa-picture-o fa-fw w3-margin-left w3-xlarge w3-text-teal"></i>
                <div class="image-upload" style="margin-top: 0; display: none;">
                    <input id="file-input" type="file" name="photo" size="500">
                </div>
            </label>
        </div>
        <div style=" display: inline-block;
                        position:absolute;
                        width: 10%">
            <input type="text" name="friendID" hidden value="${friendID}">
            <input type="submit" class="w3-button" style="background-color: #467ed9;
                                                        color: #ffffff;
                                                        margin-top: 10%;
                                                        margin-left: 15%">
        </div>
    </form>
</div>
<jsp:include page="/WEB-INF/template/footer.jsp"/>