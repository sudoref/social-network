<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../template/header.jsp"/>
<div class="w3-col">

    <div class="w3-container w3-card w3-white w3-margin-bottom" style="width:600px; margin: 4% auto auto;">
        <h3 class="w3-text-grey w3-padding-16"><i
                class="fa fa-star fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Moderators</h3>
        <hr>
        <c:forEach items="${moderators}" var="member">
            <form action="handlemembership" method="post">
                <input type="hidden" name="group" value="${param.name}">
                <input type="hidden" name="member" value="${member.getAccountId()}">
                <h4 class="w3-text-grey w3-padding-8">
                    <a href="${getContextPath}/account?id=${member.getAccountId()}">
                        <i class="fa fa-user fa-fw w3-margin-right w3-text-teal"></i>${member.getFirstName()} ${account.getSurname()}
                    </a>
                </h4><input type="hidden" name="member" value="${member.getAccountId()}">
                <c:if test="${isModerator}">
                    <input type="submit" name="handleStatus" value="remove moderator" class="w3-button-blue" style="
                                                        margin-left: 60%;
                                                        margin-top: -5%;
                                                        ">
                </c:if>
                <br>
            </form>
        </c:forEach>
    </div>

    <div class="w3-container w3-card w3-white w3-margin-bottom " style="width:600px; margin: 4% auto auto;">
        <h3 class="w3-text-grey w3-padding-16"><i
                class="fa fa-users fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Members</h3>
        <hr>
        <c:forEach items="${members}" var="member">
            <form action="handlemembership" method="post">
                <input type="hidden" name="group" value="${param.name}">
                <input type="hidden" name="member" value="${member.getAccountId()}">
                <h4 class="w3-text-grey w3-padding-8">
                    <a href="${getContextPath}/account?id=${member.getAccountId()}">
                        <i class="fa fa-user fa-fw w3-margin-right w3-text-teal"></i>${member.getFirstName()} ${account.getSurname()}
                    </a><input type="hidden" name="member" value="${member.getAccountId()}">
                </h4>
                <c:if test="${isModerator}">
                    <input type="submit" name="handleStatus" value="remove member" class="w3-button-blue" style="
                                                                                    margin-left: 60%;
                                                                                    margin-top: -5%;
                                                                                    ">
                    <input type="submit" name="handleStatus" value="add moderator" class="w3-button-blue" style="
                                                                                    margin-left: 60%;
                                                                                    ">
                </c:if>
                <br>
            </form>
        </c:forEach>
    </div>
    <c:if test="${isModerator}">
        <div class="w3-container w3-card w3-white w3-margin-bottom " style="width:600px; margin: 4% auto auto;">
            <h3 class="w3-text-grey w3-padding-16"><i
                    class="fa fa-male fa-female fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Wants to join</h3>
            <hr>
            <c:forEach items="${required}" var="member">
                <form action="handlemembership" method="post">
                    <input type="hidden" name="group" value="${param.name}">
                    <input type="hidden" name="member" value="${member.getAccountId()}">
                    <h4 class="w3-text-grey w3-padding-8">
                        <a href="${getContextPath}/account?id=${member.getAccountId()}">
                            <i class="fa fa-user fa-fw w3-margin-right w3-text-teal"></i>${member.getFirstName()} ${account.getSurname()}
                        </a>
                    </h4>   <input type="hidden" name="member" value="${member.getAccountId()}">
                    <input type="submit" name="handleStatus" value="accept" class="w3-button-blue" style="
                                                                                    margin-left: 60%;
                                                                                    margin-top: -5%;
                                                                                    ">
                    <input type="submit" name="handleStatus" value="decline" class="w3-button-blue" style="
                                                                                    margin-left: 60%;
                                                                                    ">
                    <br>
                </form>
            </c:forEach>
        </div>
    </c:if>
</div>
<jsp:include page="../template/footer.jsp"/>