<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<style>
    .image-upload > input {
        display: none;
    }

    .image-upload img {
        width: 100px;
        height: 100px;
        cursor: pointer;
    }
</style>
<jsp:include page="/WEB-INF/template/header.jsp"/>
<div class="w3-container w3-card w3-white" style="width:650px; margin: 4% auto auto;">
    <div class="col-sm-15 col-sm-offset-0">
        <form method="post" action="/creatingCommunity" name="editform" enctype="multipart/form-data">
            <div class="col-sm-4 col-sm-offset-0">
                <div class="picture-container">
                    <label for="file-input">
                        <img src="https://www.writtlehayandstraw.com/wp-content/uploads/no-image-large11.png"
                             title="" style="width: 180px; height: 180px"/>
                        <div class="image-upload" style="margin-top: 0; display: none;">
                            <input id="file-input" type="file" name="photo" size="50">
                        </div>
                    </label>
                </div>
            </div>

            <div class="col-sm-6 ">
                <div class="form-group" style="width: 100%; margin-top: 10%">
                    <label>Community Name</label>
                    <input type="text" class="form-control from-reg" name="name">
                </div>
            </div>
            <div class="col-sm-10 col-sm-offset-2">
                <div class="form-group">
                    <label>Description</label>
                    <p><textarea rows="10" cols="45" name="description"></textarea></p>
                </div>
            </div>
            <div class="col-sm-6 ">
                <input type="submit" name="submit" value="Create " class="w3-button "
                       style="width: 50%; margin-bottom: 10%; margin-left: 160%; background-color: #467ed9; color: #ffffff; ">
            </div>
            <br><br>
        </form>
    </div>
</div>
<jsp:include page="/WEB-INF/template/footer.jsp"/>