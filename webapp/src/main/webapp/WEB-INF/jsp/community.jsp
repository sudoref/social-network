<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="javatime" uri="http://sargue.net/jsptags/time" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="../template/header.jsp"/>
<!-- Page Container -->
<div class="w3-content w3-margin-top" style="max-width:1400px;">
    <!-- The Grid -->
    <div class="w3-row-padding" style="width: 800px">
        <!-- Left Column -->
        <div class="w3-third">
            <div class="w3-white w3-text-grey w3-card-4">
                <div class="w3-container">
                    <h4 class="w3-text-grey w3-padding-8 w3-margin-left" style="margin-top: 10%">
                        <a href="${getContextPath}/members?name=${group.name}"><i
                                class="fa fa-users fa-fw w3-text-teal"></i>Members
                        </a>
                    </h4>
                </div>
                <hr>
                <div class="w3-container">
                    <c:forEach items="${members}" var="account">
                        <h6 class="w3-text-grey w3-padding-8">
                            <a href="${getContextPath}/account?id=${account.getAccountId()}">
                                <i class="fa fa-user fa-fw w3-margin-right w3-text-teal"></i>${account.getFirstName()} ${account.getSurname()}
                            </a>
                        </h6>
                    </c:forEach>
                </div>
                <br>
            </div>
            <br>
            <!-- End Left Column -->
        </div>
        <!-- Right Column -->
        <div class="w3-twothird">
            <div class="w3-container w3-card w3-white w3-margin-bottom" style="width: 100%">
                <div class="w3-container" style="margin-left: 7%; margin-top: 2%; margin-bottom: 2%">
                    <img src="${pageContext.request.contextPath}/printGroup?groupId=${group.name}" width="80%"
                         height="80%" alt="GroupAvatar"/>
                </div>
            </div>

            <div class="w3-container w3-card w3-white w3-margin-bottom">
                <h3 class="w3-text-grey w3-padding-16"><i
                        class="fa fa-info-circle fa-fw w3-xxlarge w3-text-teal"></i>${group.name}
                </h3>
                <form method="post" action="handlemembership">
                    <input type="hidden" name="group" value="${group.name}">
                    <c:choose>
                        <c:when test="${role.equals('member') || role.equals('moderator')}">
                            <input type="submit" name="membershipReq" value="leave community" class="w3-button-blue"
                                   style="
                                                        margin-left: 60%;
                                                        margin-top: -5%;
                                                        ">
                        </c:when>
                        <c:when test="${role.equals('requested')}">
                            <input type="submit" name="membershipReq" value="remove request" class="w3-button-blue"
                                   style="
                                                        margin-left: 60%;
                                                        margin-top: -5%;
                                                        ">
                        </c:when>
                        <c:otherwise>
                            <input type="submit" name="membershipReq" value="join community" class="w3-button-blue"
                                   style="
                                                        margin-left: 60%;
                                                        margin-top: -5%;
                                                        ">
                        </c:otherwise>
                    </c:choose>
                </form>
                <hr>
                <div class="w3-container">
                    <p><i class="fa fa-envelope fa-fw w3-margin-right w3-large w3-text-teal"></i>
                        description: ${group.description}
                    </p>
                    <br>
                </div>
            </div>
            <c:if test="${not empty role && !role.equals('requested')}">
                <div class="w3-container w3-card w3-white" style="width: 100%">
                    <h3 class="w3-text-grey w3-padding-16"><i
                            class="fa fa-users fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Community Wall
                    </h3>
                        <%--post messages--%>

                    <form id="form1" method="post" action="postMessages" enctype="multipart/form-data">
                        <div style=" display: inline-block;position:relative; margin-top: 2%">
                            <label style="position:relative;">
                                <textarea name="newMessage" cols="50" rows="3"
                                          style=" resize: none; margin-right: 1%"></textarea>
                            </label>
                            <label for="file-input" style="position:absolute;
                                        bottom:10px;
                                        right:10px;">
                                <i class="fa fa-picture-o fa-fw w3-margin-left w3-xlarge w3-text-teal"></i>
                                <div class="image-upload" style="margin-top: 0; display: none;">
                                    <input id="file-input" type="file" name="photo" size="50">
                                </div>
                            </label>
                        </div>

                        <div style="
                        position:relative;
                        width: 10%">
                            <input type="hidden" name="groupID" value="${group.groupID}">
                            <input type="hidden" name="id" value="${group.name}">
                            <input type="submit" value="Publish" class="w3-button" style="background-color: #467ed9;
                                                        color: #ffffff;
                                                        margin-top: 10%;
                                                        margin-left: 15%;
                                                        margin-bottom: 10%">
                        </div>
                    </form>

                    <h2 class="w3-text-grey w3-padding-10 w3-xlarge"><i
                            class="fa fa-id-card-o fa-fw w3-xlarge w3-marging-left w3-text-teal"></i>Posts
                    </h2>
                    <hr>
                        <%--show/delete messages--%>
                    <c:forEach items="${messages}" var="post">
                        <form id="post" action="postMessages" method="post">
                            <h5 class="w3-text-grey w3-padding-6" style="margin-bottom: 0">
                                <a href="${pageContext.request.contextPath}/account?id=${post.key.getSender().getAccountId()}">
                                    <img src="${pageContext.request.contextPath}/printAvatar?accountId=${post.key.getSender()}"
                                         style="width: 8%; height: 8%"/>
                                        ${post.value.getFirstName()}
                                </a>
                            </h5>
                            <i style="margin-left: 10%">${post.key.getText()}</i>
                            <c:if test="${not empty post.key.getImage()}">
                                <img src="${pageContext.request.contextPath}/printMessage?messageId=${post.key.getId()}"
                                     style="width: 50%; height: 50%"/>
                            </c:if>
                            <javatime:parseLocalDateTime value="${post.key.getRegistrationDate()}"
                                                         pattern="yyyy-MM-dd'T'HH:mm:ss"
                                                         var="parsedDate"/>
                            <span class="time-right"> <javatime:format value="${parsedDate}"
                                                                       pattern="HH:mm:ss dd-MM-yyyy"/></span>
                            <div style="
                        position:relative;
                        width: 10%">
                                <input type="hidden" name="groupID" value="${group.groupID}">
                                <input type="hidden" name="id" value="${group.name}">
                                <input type="hidden" name="messageIDToRemove" value="${post.key.getId()}">
                                <input type="submit" value="delete post" class="w3-button" style="background-color: #467ed9;
                                                        color: #ffffff;
                                                        margin-top: 10%;
                                                        margin-left: 15%;
                                                        margin-bottom: 10%">
                            </div>
                        </form>
                    </c:forEach>
                </div>
            </c:if>
            <!-- End Right Column -->
        </div>
        <!-- End Grid -->
    </div>
    <!-- End Page Container -->
</div>
<jsp:include page="../template/footer.jsp"/>