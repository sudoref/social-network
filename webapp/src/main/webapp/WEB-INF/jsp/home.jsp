<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="javatime" uri="http://sargue.net/jsptags/time" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>

<jsp:include page="../template/header.jsp"/>

<body class="w3-light-grey">

<!-- Page Container -->
<div class="w3-content w3-margin-top" style="max-width:1400px;">
    <!-- The Grid -->
    <div class="w3-row-padding">
        <!-- Left Column -->
        <div class="w3-third" style="width: 300px">
            <div class="w3-white w3-text-grey w3-card-4">
                <div class="w3-display-container">
                    <img src="${pageContext.request.contextPath}/printAvatar?accountId=${account.accountId}"
                         style="width:100%"
                         alt="Avatar">
                </div>
                <a href="${pageContext.request.contextPath}/toXML/${account.accountId}" class="w3-button">To XML</a>
                <hr>
                <div class="w3-container">
                    <form method="post">
                        <input hidden name="edit" value="edit">
                        <button name="edit" value="edit" type="submit" class="w3-button"
                                style="width: 100%; margin-bottom: 1%">
                            Edit
                        </button>
                    </form>
                    <p><i class="fa fa-birthday-cake fa-fw w3-margin-right w3-large w3-text-teal"></i>
                        <fmt:parseDate value="${account.birthDate}" pattern="yyyy-MM-dd" var="parsedBirthDate"
                                       type="date"/>
                        birthday: <fmt:formatDate value="${parsedBirthDate}" pattern="dd-MM-yyyy"/>
                    </p>

                    <p><i class="fa fa-calendar fa-fw w3-margin-right w3-large w3-text-teal"></i>
                        <fmt:parseDate value="${account.registrationDate}" pattern="yyyy-MM-dd"
                                       var="parsedRegistrationDate"
                                       type="date"/>
                        registration :
                        <fmt:formatDate value="${parsedRegistrationDate}" pattern="dd-MM-yyyy"/>

                        <c:choose>
                        <c:when test="${account.gender.equals('Male')}">
                    <p><i class="fa fa-mars fa-fw w3-margin-right w3-large w3-text-teal"></i> Male</p>
                    </c:when>
                    <c:otherwise>
                        <p><i class="fa fa-venus fa-fw w3-margin-right w3-large w3-text-teal"> </i>Female</p>
                    </c:otherwise>
                    </c:choose>

                    <hr>
                    <h4 class="w3-text-grey w3-padding-8"><i
                            class="fa fa-handshake-o fa-fw w3-margin-right w3-text-teal"></i><a
                            href="${pageContext.request.contextPath}/friendList?id=${param.id}">My
                        Friends</a>
                    </h4>
                    <c:if test="${not empty friendList}">
                        <c:forEach items="${friendList}" var="friend">
                            <h6 class="w3-text-grey w3-padding-8"><a
                                    href="${pageContext.request.contextPath}/account?id=${friend.getAccountId()}">
                                <i class="fa fa-user fa-fw w3-margin-right w3-text-teal"></i>${friend.getFirstName()} ${friend.getSurname()}
                            </a>
                            </h6>
                        </c:forEach>
                    </c:if>
                </div>
            </div>
            <br>
            <!-- End Left Column -->
        </div>

        <!-- Right Column -->
        <div class="w3-twothird">
            <div class="w3-container w3-card w3-white w3-margin-bottom">
                <h2>${account.firstName} ${account.surname}
                </h2>

                <hr>
                <div class="w3-container">

                    <p><i class="fa fa-envelope fa-fw w3-margin-right w3-large w3-text-teal"></i>
                        e-mail: ${account.email}
                    </p>

                    <c:forEach items="${account.phones}" var="phone">
                    <c:if test="${phone.getType().name().equals('MOB')}">
                    <p><i class="fa fa-mobile fa-fw w3-margin-right w3-large w3-text-teal"></i>Mobile:
                            ${phone.getNumber()}
                        </c:if>
                        <c:if test="${phone.getType().name().equals('HOME')}">
                    <p><i class="fa fa-phone fa-fw w3-margin-right w3-large w3-text-teal"></i>Home:
                            ${phone.getNumber()}
                        </c:if>
                        <c:if test="${phone.getType().name().equals('WORK')}">
                    <p><i class="fa fa-phone-square fa-fw w3-margin-right w3-large w3-text-teal"></i>Work:
                            ${phone.getNumber()}
                        </c:if>
                        </c:forEach>

                    <p><i class="fa fa-briefcase fa-fw w3-margin-right w3-large w3-text-teal"></i>
                        work address: ${account.workAddress}
                    </p>
                    <p><i class="fa fa-home fa-fw w3-margin-right w3-large w3-text-teal"></i>
                        home address: ${account.homeAddress}
                    </p>
                    <br>
                </div>
            </div>
            <%--Communities--%>
            <div class="w3-container w3-card w3-white w3-margin-bottom" style="width: 400px">
                <h2 class="w3-text-grey w3-padding-10 w3-xlarge"><i
                        class="fa fa-users fa-fw w3-margin-left w3-xlarge w3-text-teal"></i>Communities
                </h2>
                <c:if test="${not empty param.communityError}">
                    Community with this name is already exists!
                </c:if>
                <div class="wizard-footer text-center ">
                    <form method="post" action="createcommunity">
                        <button type="submit" class="w3-button"
                                style="width: 55%; background-color: #467ed9; color: #ffffff; margin-left: 5% ">
                            Create community
                        </button>
                    </form>
                </div>
                <hr>
                <c:forEach begin="0" end="2" items="${groups}" var="group">
                    <h3 class="w3-text-grey w3-padding-8 w3-large w3-margin-left">
                        <a href="${getContextPath}/community?name=${group.getName()}">
                            <i class="fa fa-users fa-fw w3-margin-right w3-text-teal"></i>${group.getName()}
                        </a>
                    </h3>
                </c:forEach>
            </div>
            <%--Post messages--%>
            <div class="w3-container w3-card w3-white w3-margin-bottom" style="width: 600px">
                <form id="form1" method="post" action="postMessages" enctype="multipart/form-data">
                    <div style=" display: inline-block;position:relative; margin-top: 2%">
                        <label style="position:relative;">
                            <textarea name="newMessage" maxlength="30" cols="60" rows="3"
                                      style=" resize: none; margin-right: 1%"></textarea>
                        </label>
                        <label for="file-input" style="position:absolute;
                                        bottom:10px;
                                        right:10px;">
                            <i class="fa fa-picture-o fa-fw w3-margin-left w3-xlarge w3-text-teal"></i>
                            <div class="image-upload" style="margin-top: 0; display: none;">
                                <input id="file-input" type="file" name="photo" size="50">
                            </div>
                        </label>
                    </div>

                    <div style="
                        position:relative;
                        width: 10%">
                        <input type="hidden" name="id" value="${param.id}">
                        <input type="submit" value="Publish" class="w3-button" style="background-color: #467ed9;
                                                        color: #ffffff;
                                                        margin-top: 10%;
                                                        margin-left: 15%;
                                                        margin-bottom: 10%">
                    </div>
                </form>

                <h2 class="w3-text-grey w3-padding-10 w3-xlarge"><i
                        class="fa fa-id-card-o fa-fw w3-xlarge w3-marging-left w3-text-teal"></i>Posts
                </h2>
                <hr>
                <c:forEach items="${posts}" var="post">

                    <h5 class="w3-text-grey w3-padding-6" style="margin-bottom: 0">
                        <a href="${pageContext.request.contextPath}/account?id=${post.key.getSender().getAccountId()}">
                            <img src="${pageContext.request.contextPath}/printAvatar?accountId=${post.value.getAccountId()}"
                                 style="width: 8%; height: 8%">
                                ${post.value.getFirstName()}
                        </a>
                    </h5>
                    <i style="margin-left: 10%">${post.key.getText()}</i>
                    <c:if test="${not empty post.key.getImage()}">
                        <img src="${pageContext.request.contextPath}/printMessage?messageId=${post.key.getId()}"
                             style="width: 50%; height: 50%"/>
                    </c:if>
                    <javatime:parseLocalDateTime value="${post.key.getRegistrationDate()}"
                                                 pattern="yyyy-MM-dd'T'HH:mm:ss"
                                                 var="parsedDate"/>
                    <span class="time-right"> <javatime:format value="${parsedDate}"
                                                               pattern="HH:mm:ss dd-MM-yyyy"/></span>
                    <form id="post" action="postMessages" method="post">
                        <div style="
                        position:relative;
                        width: 10%">
                            <input type="hidden" name="id" value="${param.id}">
                            <input type="hidden" name="messageIDToRemove" value="${post.key.getId()}">
                            <input type="submit" value="delete post" class="w3-button" style="background-color: #467ed9;
                                                        color: #ffffff;
                                                        margin-top: 10%;
                                                        margin-left: 15%;
                                                        margin-bottom: 10%">
                        </div>
                    </form>
                </c:forEach>
            </div>
            <!-- End Right Column -->
        </div>
        <!-- End Grid -->
    </div>
    <!-- End Page Container -->
</div>

<jsp:include page="../template/footer.jsp"/>