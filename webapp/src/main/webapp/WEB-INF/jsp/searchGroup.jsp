<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="../template/header.jsp"/>

<div class="w3-container w3-card w3-white" style="width:600px; margin: 4% auto auto;">
    <br>
    <h2 class="w3-text-grey w3-padding-10 w3-xlarge"><i
            class="fa fa-search fa-fw w3-margin-left w3-xlarge w3-text-teal"></i>Search results
    </h2>
    <nav id="header" class="navbar navbar-light bg-light border-bottom border-light navbar-expand-lg">
        <div class="row justify-content-md-center">
            <ul class="nav nav-tabs justify-content-md-center nsw-tnr-nav" role="tablist">
                <li class="nav-item ">
                    <a class="nav-link" href="${pageContext.request.contextPath}/search?search=${param.search}"
                       role="tab"><i class="fa fa-user fa-fw  w3-xlarge w3-text-teal"></i>
                        Accounts</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"
                       href="${pageContext.request.contextPath}/searchCommunity?search=${param.search}"
                       role="tab" style="background-color:  #dbdbdb"><i
                            class="fa fa-users fa-fw  w3-xlarge w3-text-teal"></i>Communities</a>
                </li>
            </ul>
        </div>
    </nav>
    <input id="searchPattern" value="${searchPattern}" type="hidden"/>
    <input id="objectQty" value="${objectQty}" type="hidden"/>
    <div class="w3-margin-left col-md-12">
        <div id="searchResult"></div>
    </div>
    <div class="w3-centered w3-padding-10">
        <ul id="nsw-pagination" class="pagination nsw-tnr-pagination"></ul>
    </div>

</div>
<jsp:include page="../template/footer.jsp"/>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="webjars/twbs-pagination/1.4.1/jquery.twbsPagination.min.js"></script>
<script>
    <%@include file="/resources/js/searchGroup.js" %>
</script>
