<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
    <title>Spring Boot WebSocket</title>
    <style>
        <%@include file="../../resources/css/main.css" %>
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.1.4/sockjs.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>

</head>
<jsp:include page="../template/header.jsp"/>

<div id="chat-container" style="height: 700px; width:600px; margin: 4% auto auto;">

    <span id="username" hidden>${username}</span>

    <div id="connecting">Connecting...</div>
    <ul id="messageArea">
    </ul>
    <form id="messageForm" name="messageForm">
        <div class="input-message" style=" display: inline-block; position:relative;">
            <input type="text" id="message" autocomplete="off"
                   placeholder="Type a message..."/>
            <button type="submit">Send</button>
        </div>
    </form>
</div>

<script>
    <%@include file="/resources/js/messenger.js" %>
</script>
<jsp:include page="../template/footer.jsp"/>
