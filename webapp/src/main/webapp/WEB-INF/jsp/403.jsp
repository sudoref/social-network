<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>

<jsp:include page="../template/header.jsp"/>

<body class="w3-light-grey">
<div class="w3-content w3-margin-top" style="max-width:1400px;">
    <h1>Ошибка 403</h1><br>
    <h3>У вас нет доступа.</h3>
</div>

<jsp:include page="../template/footer.jsp"/>