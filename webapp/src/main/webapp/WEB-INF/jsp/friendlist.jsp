<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="../template/header.jsp"/>
<style>
    .image-upload > input {
        display: none;
    }

    .image-upload img {
        width: 100px;
        height: 100px;
        cursor: pointer;
    }
</style>

<div class="w3-container w3-card w3-white" style="max-width:800px; margin: 4% auto auto;">
    <c:if test="${empty relationship}">
        <h1>
            Хватит сидеть за компом, иди найди себе друзей!
        </h1>
    </c:if>
    <h1 class="w3-text-grey w3-padding-10 w3-xlarge " style="margin-top: 2%"><i
            class="fa fa-handshake-o fa-fw w3-margin-left w3-xlarge w3-text-teal"></i> My Friends
    </h1>

    <c:forEach items="${relationship}" var="friend">

        <c:choose>
            <c:when test="${friend.value == 'possibleFriend'}">
                <div class="w3-container w3-card w3-white ">
                    <div class="card wizard-card ct-wizard-orange" id="wizard" style="width: 600px">
                        <div class="col-sm-3">
                            <div class="picture-container">
                                <div class="image-upload" style="margin-top: 20px;">
                                    <a href="${pageContext.request.contextPath}/account?id=${friend.key.getAccountId()}">
                                        <img src="${pageContext.request.contextPath}/printAvatar?accountId=${friend.key.getAccountId()}"
                                             style="width: 50%; height: 50%">
                                    </a>
                                </div>
                                <br>
                            </div>
                        </div>
                        <div class="col-sm-7 ">

                            <h3 class="w3-text-grey w3-padding-8">
                                <i class="fa fa-user fa-fw w3-margin-right w3-text-teal"></i>${friend.key.getFirstName()} ${friend.key.getSurname()}
                            </h3>
                            wants to be your friend
                            <form action="accept" method="post">
                                <button class="w3-button" name="accept" value="${friend.key.getAccountId()}"
                                        style="display: inline-block; margin-right: 2%">
                                    Accept
                                </button>
                                <button class="w3-button" name="deny" value="${friend.key.getAccountId()}"
                                        style="display: inline-block">
                                    Deny
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:when test="${friend.value == 'requestedFriend'}">
                <div class="w3-container w3-card w3-white ">
                    <div class="card wizard-card ct-wizard-orange" style="width: 600px">
                        <div class="col-sm-3">
                            <div class="picture-container">
                                <div class="image-upload" style="margin-top: 20px;">
                                    <a href="${pageContext.request.contextPath}/account?id=${friend.key.getAccountId()}">
                                        <img src="${pageContext.request.contextPath}/printAvatar?accountId=${friend.key.getAccountId()}"
                                             style="width: 50%; height: 50%">
                                    </a>
                                </div>
                                <br>
                            </div>
                        </div>
                        <div class="col-sm-7 ">
                            <h3 class="w3-text-grey w3-padding-8">
                                <i class="fa fa-user fa-fw w3-margin-right w3-text-teal"></i>${friend.key.getFirstName()} ${friend.key.getSurname()}
                            </h3>
                            friend request successfully sent
                            <form action="accept" method="post">

                                <button class="w3-button" name="deny" value="${friend.key.getAccountId()}"
                                        style="display: inline-block">
                                    Remove request
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:when test="${friend.value == 'friend'}">
                <div class="w3-container w3-card w3-white ">
                    <div class="card wizard-card ct-wizard-orange" style="width: 600px">
                        <div class="col-sm-3">
                            <div class="picture-container">
                                <div class="image-upload" style="margin-top: 20px;">
                                    <a href="${pageContext.request.contextPath}/account?id=${friend.key.getAccountId()}">
                                        <img src="${pageContext.request.contextPath}/printAvatar?accountId=${friend.key.getAccountId()}"
                                             style="width: 50%; height: 50%">
                                    </a>
                                </div>
                                <br>
                            </div>
                        </div>
                        <div class="col-sm-7 ">
                            <h3 class="w3-text-grey w3-padding-8">
                                <i class="fa fa-user fa-fw w3-margin-right w3-text-teal"></i>${friend.key.getFirstName()} ${friend.key.getSurname()}
                            </h3>

                            <form action="accept" method="post">

                                <button class="w3-button" name="deny" value="${friend.key.getAccountId()}"
                                        style="display: inline-block">
                                    Remove friend
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </c:when>
        </c:choose>
    </c:forEach>
    <br>
    <br>
</div>


