create table accounts
(
	ID int auto_increment
		primary key,
	firstName varchar(255) null charset utf8,
	midName varchar(255) null charset utf8,
	surname varchar(255) null charset utf8,
	birthDate date null,
	homeAddress varchar(255) null charset utf8,
	workAddress varchar(255) null charset utf8,
	email varchar(255) not null charset utf8,
	icq bigint null,
	skype varchar(255) null charset utf8,
	gender varchar(255) null charset utf8,
	password varchar(45) not null charset utf8,
	registrationDate varchar(45) null charset utf8,
	isAdmin tinyint(2) default '0' not null,
	avatar longblob null,
	constraint ID_UNIQUE
		unique (ID),
	constraint email_UNIQUE
		unique (email)
)

create table group_membership
(
	group_id int null,
	account_id int null,
	status int null,
	constraint group_membership_accounts_ID_fk
		foreign key (account_id) references accounts (ID)
)

create table groups
(
	ID int auto_increment,
	description varchar(45) null,
	creator varchar(45) null,
	name varchar(45) null,
	avatar longblob null,
	constraint ID_UNIQUE
		unique (ID),
	constraint name_UNIQUE
		unique (name)
)


create table messages
(
	conversationID varchar(45) null charset utf8,
	type enum('PRIVATE', 'GROUP', 'POST') null,
	sender int null,
	date datetime null,
	message text null charset utf8,
	recipient int null,
	image longblob null,
	ID int auto_increment,
	constraint ID_UNIQUE
		unique (ID),
	constraint fk_messages_2
		foreign key (sender) references accounts (ID)
			on update cascade on delete cascade
)


create table phones
(
	type enum('MOB', 'WORK', 'HOME') not null,
	number bigint not null,
	accountID int not null,
	ID int unsigned auto_increment
		primary key,
	constraint phones_ibfk_1
		foreign key (accountID) references accounts (ID)
			on update cascade on delete cascade
)

create table relationship
(
	account_one_id int not null,
	account_two_id int not null,
	status tinyint not null,
	action_account_id int not null,
	constraint fk_relationship_1
		foreign key (account_one_id) references accounts (ID)
			on update cascade on delete cascade,
	constraint fk_relationship_2
		foreign key (account_two_id) references accounts (ID)
			on update cascade on delete cascade,
	constraint fk_relationship_3
		foreign key (action_account_id) references accounts (ID)
			on update cascade on delete cascade
)

