package com.getjavajob.training.web1803.socnetwork.sudarev.service;

import com.getjavajob.training.web1803.socnetwork.sudarev.common.Account;
import com.getjavajob.training.web1803.socnetwork.sudarev.common.Relationship;
import com.getjavajob.training.web1803.socnetwork.sudarev.dao.interfaces.RelationshipDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RelationshipService {

    @Autowired
    private RelationshipDao relationshipDao;

    @Transactional
    public Relationship friendRequest(Account initiator, Account possFriend) {
        return relationshipDao.save(new Relationship(initiator.getAccountId(), possFriend.getAccountId(), 1, initiator.getAccountId()));
    }

    @Transactional
    public void removeFromFriends(Account initiator, Account exFriend) {
        Relationship relationship = getCorrectRelationship(initiator.getAccountId(),exFriend.getAccountId());
        relationshipDao.delete(relationship.getAccount1(),relationship.getAccount2());
    }

    @Transactional
    public void handleStatus(Account initiator, Account account, int status) {
        Relationship relationship = getCorrectRelationship(account.getAccountId(),initiator.getAccountId());
        relationship.setStatus(status);
        relationship.setInitiator(initiator.getAccountId());
        relationshipDao.save(relationship);
    }

    @Transactional
    public List<Integer> getFriendList(Account account) {
        List<Integer> friends = new ArrayList<>();
        List<Relationship> list = relationshipDao.getAllByAccount(account.getAccountId());
        for (Relationship relationship : list) {
            if (!relationship.getAccount1().equals(account.getAccountId())) {
                friends.add(relationship.getAccount1());
            } else {
                friends.add(relationship.getAccount2());
            }
        }
        return friends;
    }

    @Transactional
    public Map<Integer, int[]> getAllRelationships(Account account) {
        Map<Integer, int[]> relationships = new HashMap<>();
        int accountId = account.getAccountId();
        List<Relationship> list = relationshipDao.getAllByAccount(accountId);
        for (Relationship relationship : list) {
            int[] status = new int[2];
            if (relationship.getAccount1() == accountId) {
                status[0] = relationship.getStatus();
                status[1] = relationship.getInitiator();
                relationships.put(relationship.getAccount2(), status);
            } else {
                status[0] = relationship.getStatus();
                status[1] = relationship.getInitiator();
                relationships.put(relationship.getAccount1(), status);
            }
        }
        return relationships;
    }

    private Relationship getCorrectRelationship(int account1, int account2){
        if(account1<account2){
            return relationshipDao.getByAccount1AndAccount2(account1,account2);
        } else {
            return relationshipDao.getByAccount1AndAccount2(account2,account1);
        }
    }
}
