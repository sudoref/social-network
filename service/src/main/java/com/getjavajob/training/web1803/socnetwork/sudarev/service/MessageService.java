package com.getjavajob.training.web1803.socnetwork.sudarev.service;

import com.getjavajob.training.web1803.socnetwork.sudarev.common.Message;
import com.getjavajob.training.web1803.socnetwork.sudarev.dao.interfaces.MessageDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MessageService {

    @Autowired
    private MessageDao messageDao;

    @Transactional
    public Message sendPrivateMessage(Message message) {
        message.setConversation(getConversationID(message.getSender().getAccountId(), message.getRecipient()));
        return messageDao.save(message);
    }

    @Transactional
    public Message sendPostMessage(Message message) {
        return messageDao.save(message);
    }

    public List<Message> getAllPostMessagesFromAccount(int recipient) {
        List<Message> posts = messageDao.getAllByTypeAndConversation(Message.Type.valueOf("POST"), String.valueOf(recipient));
        posts.sort((message1, message2) -> message1.getId() < message2.getId() ? -1 : 1);
        return posts;
    }

    public List<Message> receivePrivateConversation(int accountID1, int accountID2) {
        String conversationID = getConversationID(accountID1, accountID2);
        return messageDao.getAllByTypeAndConversation(Message.Type.valueOf("PRIVATE"), conversationID);
    }

    @Transactional
    public void deletePostFromAccount(Message message) {
        messageDao.delete(message);
    }

    @Transactional
    public List<Message> getAllGroupMessages(String groupName) {
        return messageDao.getAllByTypeAndConversation(Message.Type.valueOf("GROUP"), groupName);
    }

    @Transactional
    public Message getMessageByKey(Integer key) {
        return messageDao.findById(key).orElseThrow(NullPointerException::new);
    }

    private String getConversationID(int accountId1, int accountId2) {
        if (accountId1 < accountId2) {
            return accountId1 + ":" + accountId2;
        } else {
            return accountId2 + ":" + accountId1;
        }
    }
}
