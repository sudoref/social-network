package com.getjavajob.training.web1803.socnetwork.sudarev.service;

import com.getjavajob.training.web1803.socnetwork.sudarev.common.Account;
import com.getjavajob.training.web1803.socnetwork.sudarev.dao.interfaces.AccountDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AccountService {
    private static final Logger logger = LoggerFactory.getLogger(AccountService.class);
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AccountDao accountDao;

    @Transactional
    public Account createAccount(Account account) {
        logger.debug("Create new account with email = {}", account.getEmail());
        String pass = account.getPassword();
        account.setPassword(passwordEncoder.encode(pass));
        return accountDao.save(account);
    }

    public Account getAccountByEmail(String email) {
        logger.debug("Get account with email = {}", email);
        return accountDao.getAccountByEmail(email);
    }

    public Account getAccountById(Integer id) {
        logger.debug("Get account with id = {}", id);
        return accountDao.getAccountByAccountId(id);
    }

    @Transactional
    @Secured("ROLE_ADMIN")
    public int deleteAccount(Account account) {
        logger.debug("Delete account with id = {}", account.getAccountId());
        accountDao.delete(account);
        return account.getAccountId();
    }

    @Transactional
    public void update(Account account) {
        logger.debug("Update account with id = {}", account.getAccountId());
        accountDao.save(account);
    }

    public int getAccountQty(String pattern) {
        logger.debug("Accounts list size with pattern = '{}'", pattern);
        return accountDao.countAccountsByEmail(pattern + '%');
    }

    public List<Account> searchByPage(String searchPattern, int page, int maxResult) {
        return accountDao.getByPage(searchPattern + '%', PageRequest.of(page, maxResult));
    }

    public List<Account> searchAccounts(String pattern) {
        logger.debug("Account search with pattern = '{}'", pattern);
        return accountDao.getByPattern(pattern + '%');
    }
}
