package com.getjavajob.training.web1803.socnetwork.sudarev.service;

import com.getjavajob.training.web1803.socnetwork.sudarev.common.Account;
import com.getjavajob.training.web1803.socnetwork.sudarev.common.Group;
import com.getjavajob.training.web1803.socnetwork.sudarev.common.Membership;
import com.getjavajob.training.web1803.socnetwork.sudarev.dao.interfaces.AccountDao;
import com.getjavajob.training.web1803.socnetwork.sudarev.dao.interfaces.GroupDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Iterator;
import java.util.List;

@Service
public class GroupService {

    private static final Logger logger = LoggerFactory.getLogger(AccountService.class);
    @Autowired
    private GroupDao groupDao;
    @Autowired
    private AccountDao accountDao;

    @Transactional
    public Group createGroup(Group group) {
        logger.info("creating group with name={}", group.getName());
        try {
            Account account = accountDao.getAccountByEmail(group.getCreator());
            Membership membership = new Membership(account, group, 3);
            account.getGroups().add(membership);
            group.getMembers().add(membership);
            return groupDao.save(group);
        } catch (NullPointerException e) {
            return null;
        }
    }

    public int getGroupQty(String pattern) {
        logger.info("getting quantity of groups by pattern={}", pattern);
        return groupDao.countAllByNameLike(pattern + '%');
    }

    @Transactional
    public void updateGroup(Group group) {
        groupDao.save(group);
    }

    public Group getGroupByName(String groupName) {
        logger.info("getting group by name={}", groupName);
        return groupDao.getByName(groupName);
    }

    public List<Group> searchByPages(String searchPattern, int page, int maxResult) {
        logger.info("pagination searching group by pattern={}", searchPattern);
        return groupDao.getAllByNameLike(searchPattern + "%", PageRequest.of(page, maxResult));
    }

    public List<Group> searchGroups(String pattern) {
        logger.info("searching group by pattern={}", pattern);
        return groupDao.getAllByNameLike(pattern + '%');
    }

    public int getMembershipStatus(Account account, Group group) {
        logger.info("getting status for account={} in group ={}", account.getEmail(), group.getName());
        int status = 0;
        for (Membership membership : group.getMembers()) {
            if (membership.getAccount().getEmail().equals(account.getEmail())) {
                status = membership.getStatus();
            }
        }
        return status;
    }

    @Transactional
    public void requestAccountForGroupMembership(Account account, Group group) {
        logger.info("sending request for group membership by account = {}", account.getEmail());
        List<Membership> members = group.getMembers();
        members.add(new Membership(account, group, 1));
        group.setMembers(members);
        groupDao.save(group);
    }

    @Transactional
    public void makeAccountAsGroupMembership(Account account, Group group) {
        logger.info("making account={} as membership of group={}", account.getEmail(), group.getName());
        List<Membership> members = group.getMembers();
        for (Membership membership : members) {
            if (membership.getGroup().equals(group) &&
                    membership.getAccount().equals(account)) {
                membership.setStatus(2);
            }
        }
        group.setMembers(members);
        groupDao.save(group);
    }

    @Transactional
    public void makeAccountGroupModerator(Account account, Group group) {
        logger.info("making account={} as group={} moderator", account.getEmail(), group.getName());
        List<Membership> members = group.getMembers();
        for (Membership membership : members) {
            if (membership.getGroup().equals(group) &&
                    membership.getAccount().equals(account)) {
                membership.setStatus(3);
            }
        }
        group.setMembers(members);
        groupDao.save(group);
    }

    @Transactional
    public void removeAccountFromGroup(Account account, Group group) {
        logger.info("removing account={} from group={}", account.getEmail(), group.getName());
        List<Membership> members = group.getMembers();
        members.removeIf(
                membership -> membership.getGroup().equals(group) &&
                membership.getAccount().equals(account));
        if (members.size() > 0) {
            group.setMembers(members);
            groupDao.save(group);
        } else {
            groupDao.delete(group);
        }
    }
}
