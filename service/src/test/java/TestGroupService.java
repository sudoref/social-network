import com.getjavajob.training.web1803.socnetwork.sudarev.common.Account;
import com.getjavajob.training.web1803.socnetwork.sudarev.common.Group;
import com.getjavajob.training.web1803.socnetwork.sudarev.dao.interfaces.GroupDao;
import com.getjavajob.training.web1803.socnetwork.sudarev.service.GroupService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

public class TestGroupService {

    @Mock
    private GroupDao gDaoMock;

    @InjectMocks
    @Spy
    private GroupService service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @Test(expected = NullPointerException.class)
    public void testUpdateGroup() {
        doThrow(new NullPointerException()).when(gDaoMock).save(any(Group.class));
        Group group = new Group();
        group.setName("test");
        service.updateGroup(group);
    }

    @Test
    public void testGetGroupByName() {
        Group group = new Group();
        group.setName("test");
        when(gDaoMock.getByName("test")).thenReturn(group);
        assertEquals(group, service.getGroupByName("test"));
    }

    @Test
    public void testSearchGroup() {
        Group group = new Group();
        List<Group> groups = new ArrayList<>();
        groups.add(group);
        group.setName("test");
        when(gDaoMock.getAllByNameLike("test%")).thenReturn(groups);
        assertEquals(1, service.searchGroups("test").size());
    }

    @Test
    public void testGetMembershipStatus() {
        Account account = new Account();
        account.setAccountId(1);
        account.setEmail("test@test.ru");
        Group group = new Group();
        group.setGroupID(1);
        group.setName("test");
        assertEquals(0, service.getMembershipStatus(account, group));
    }

    @Test(expected = NullPointerException.class)
    public void testRequestAccountForGroupMembership() {
        doThrow(new NullPointerException()).when(gDaoMock).save(any(Group.class));
        Account account = new Account();
        Group group = new Group();
        group.setName("test");
        service.requestAccountForGroupMembership(account, group);
    }

    @Test(expected = NullPointerException.class)
    public void testMakeAccountAsGroupMembership() {
        doThrow(new NullPointerException()).when(gDaoMock).save(any(Group.class));
        service.makeAccountAsGroupMembership(new Account(), new Group());
    }

    @Test(expected = NullPointerException.class)
    public void testRemoveAccountFromGroup() {
        doThrow(new NullPointerException()).when(gDaoMock).save(any(Group.class));
        service.removeAccountFromGroup(new Account(), new Group());
    }
}
