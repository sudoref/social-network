import com.getjavajob.training.web1803.socnetwork.sudarev.common.Account;
import com.getjavajob.training.web1803.socnetwork.sudarev.common.Relationship;
import com.getjavajob.training.web1803.socnetwork.sudarev.dao.interfaces.RelationshipDao;
import com.getjavajob.training.web1803.socnetwork.sudarev.service.RelationshipService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class TestRelationshipService {
    @Mock
    private RelationshipDao relationshipDao;

    @Spy
    @InjectMocks
    private RelationshipService service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFriendRequest() {
        when(relationshipDao.save(any(Relationship.class))).thenAnswer((Answer<Relationship>) invocation -> {
            Object[] arguments = invocation.getArguments();
            if (arguments != null && arguments.length > 0 && arguments[0] != null) {
                Relationship relationship = (Relationship) arguments[0];
                relationship.setAccount1(1);
                relationship.setAccount2(2);
                relationship.setStatus(1);
                relationship.setInitiator(1);
                return relationship;
            }
            return null;
        });
        Account account1 = new Account();
        Account account2 = new Account();
        account1.setAccountId(1);
        account2.setAccountId(2);
        assertEquals(1L, service.friendRequest(account1, account2).getStatus().longValue());
    }

    @Test(expected = NullPointerException.class)
    public void testRemoveFromFriend() {
        doThrow(new NullPointerException()).when(relationshipDao).delete(any(Relationship.class));
        service.removeFromFriends(new Account(), new Account());
    }

    @Test(expected = NullPointerException.class)
    public void testHandleStatus() {
        doThrow(new NullPointerException()).when(relationshipDao).save(any(Relationship.class));
        service.handleStatus(new Account(), new Account(), 0);
    }

    @Test
    public void testGetFriendList() {
        when(relationshipDao.getAllByAccount(anyInt())).thenAnswer((Answer<List<Relationship>>) invocation -> {
            Object[] arguments = invocation.getArguments();
            if (arguments != null && arguments.length > 0 && arguments[0] != null) {
                Relationship relationship = new Relationship(1, 2, 1, 1);
                List<Relationship> relationshipList = new ArrayList<>();
                relationshipList.add(relationship);
                return relationshipList;
            }
            return null;
        });
        Account account = new Account();
        account.setAccountId(1);
        assertEquals(1, service.getFriendList(account).size());
    }

    @Test
    public void testGetAllRelationships() {
        when(relationshipDao.getAllByAccount(anyInt())).thenAnswer((Answer<List<Relationship>>) invocation -> {
            Object[] arguments = invocation.getArguments();
            if (arguments != null && arguments.length > 0 && arguments[0] != null) {
                Relationship relationship = new Relationship(1, 2, 1, 1);
                List<Relationship> relationshipList = new ArrayList<>();
                relationshipList.add(relationship);
                return relationshipList;
            }
            return null;
        });
        Map<Integer, int[]> relationships = new HashMap<>();
        Account account = new Account();
        account.setAccountId(1);
        assertEquals(1, service.getAllRelationships(account).size());
    }

}
