import com.getjavajob.training.web1803.socnetwork.sudarev.common.Account;
import com.getjavajob.training.web1803.socnetwork.sudarev.common.Message;
import com.getjavajob.training.web1803.socnetwork.sudarev.dao.interfaces.MessageDao;
import com.getjavajob.training.web1803.socnetwork.sudarev.service.MessageService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class TestMessageService {

    @Mock
    private MessageDao mDaoMock;

    @Spy
    @InjectMocks
    private MessageService messageService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSendMessage() {
        when(mDaoMock.save(any(Message.class))).thenAnswer((Answer<Message>) invocation -> {
            Object[] arguments = invocation.getArguments();
            if (arguments != null && arguments.length > 0 && arguments[0] != null) {
                Message message = (Message) arguments[0];
                message.setId(1);
                message.setSender(any(Account.class));
                message.setRecipient(3);
                message.setType(Message.Type.PRIVATE);
                return message;
            }
            return null;
        });
        Message message = new Message();
        assertEquals(1L, messageService.sendPostMessage(message).getId().longValue());
    }

    @Test
    public void testGetMessages() {
        when(mDaoMock.getAllByTypeAndConversation(Message.Type.PRIVATE, "1:2")).thenAnswer((Answer<List<Message>>) invocation -> {
            Object[] arguments = invocation.getArguments();
            if (arguments != null && arguments.length > 0 && arguments[0] != null) {
                Message message = new Message();
                message.setId(1);
                message.setSender(any(Account.class));
                message.setRecipient(3);
                message.setType(Message.Type.PRIVATE);
                List<Message> messageList = new ArrayList<>();
                messageList.add(message);
                return messageList;
            }
            return null;
        });
        assertEquals(1, messageService.receivePrivateConversation(1, 2).size());
    }

    @Test(expected = NullPointerException.class)
    public void testDeletePostFromAccount() {
        doThrow(new NullPointerException()).when(mDaoMock).delete(any(Message.class));
        messageService.deletePostFromAccount(new Message());
    }

    @Test
    public void testGetMessageByKey() {
        Message message = new Message();
        when(mDaoMock.findById(anyInt())).thenReturn(Optional.of(message));
        assertNotNull(messageService.getMessageByKey(2));
    }
}
