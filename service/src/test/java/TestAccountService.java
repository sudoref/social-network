import com.getjavajob.training.web1803.socnetwork.sudarev.common.Account;
import com.getjavajob.training.web1803.socnetwork.sudarev.common.Phone;
import com.getjavajob.training.web1803.socnetwork.sudarev.dao.interfaces.AccountDao;
import com.getjavajob.training.web1803.socnetwork.sudarev.service.AccountService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

public class TestAccountService {

    @Mock
    private AccountDao aDaoMock;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    @Spy
    private AccountService service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateAccount_returnsNewAccountWithId() {
        Account account = new Account("Ivan", "Ivanovich", "Ivanov", null, null, null,
                "ivanov@mail.ru", 123321L, "asdffda", "male", null, "123qweASD");
        List<Phone> phones = new ArrayList<>();
        phones.add(new Phone(123321L, Phone.Type.MOB, account));
        phones.add(new Phone(333333L, Phone.Type.HOME, account));
        account.setPhones(phones);
        when(aDaoMock.save(account)).thenReturn(account);
        assertEquals(account, service.createAccount(account));
    }

    @Test
    public void testGetAccountByEmail() {
        Account account = new Account("Ivan", "Ivanovich", "Ivanov", null, null, null,
                "ivanov@mail.ru", 123321L, "asdffda", "male", null, "123qweASD");
        account.setAccountId(1);
        List<Phone> phones = new ArrayList<>();
        phones.add(new Phone(123321L, Phone.Type.MOB, account));
        phones.add(new Phone(333333L, Phone.Type.HOME, account));
        account.setPhones(phones);
        when(aDaoMock.getAccountByEmail("ivanov@mail.ru")).thenReturn(account);
        assertEquals(account, service.getAccountByEmail("ivanov@mail.ru"));
    }

    @Test
    public void testGetAccountById() {
        Account account = new Account("Ivan", "Ivanovich", "Ivanov", null, null, null,
                "ivanov@mail.ru", 123321L, "asdffda", "male", null, "123qweASD");
        account.setAccountId(1);
        List<Phone> phones = new ArrayList<>();
        phones.add(new Phone(123321L, Phone.Type.MOB, account));
        phones.add(new Phone(333333L, Phone.Type.HOME, account));
        account.setPhones(phones);
        when(aDaoMock.getAccountByAccountId(1)).thenReturn(account);
        assertEquals(account, service.getAccountById(1));
    }

    @Test(expected = NullPointerException.class)
    public void testDeleteAccount() {
        doThrow(new NullPointerException()).when(aDaoMock).delete(anyObject());
        Account account = new Account();
        account.setAccountId(1);
        service.deleteAccount(account);
    }

    @Test(expected = NullPointerException.class)
    public void testUpdate() {
        doThrow(new NullPointerException()).when(aDaoMock).save(any(Account.class));
        Account account = new Account();
        account.setAccountId(1);
        service.update(account);
    }

    @Test
    public void testSearchAccount() {
        List<Account> accounts = new ArrayList<>();
        Account account = new Account();
        account.setAccountId(1);
        accounts.add(account);
        when(aDaoMock.getByPattern(anyString())).thenReturn(accounts);
        assertEquals(1, service.searchAccounts("test").size());
    }
}
